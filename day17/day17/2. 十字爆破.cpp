#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
    long long n = 0, m = 0;
    scanf("%lld %lld", &n, &m);

    vector<vector<long long>> arr(n, vector<long long>(m));

    for (long long i = 0; i < n; i++)
        for (long long j = 0; j < m; j++)
            scanf("%lld", &arr[i][j]);

    vector<long long> row(n);           // 存储二维数组每一行的和
    vector<long long> col(m);           // 存储二维数组每一列的和

    for (long long i = 0; i < n; i++)   // 求每一行的和
        for (long long j = 0; j < m; j++)
            row[i] += arr[i][j];

    for (int j = 0; j < m; j++)         // 求每一列的和
        for (int i = 0; i < n; i++)
            col[j] += arr[i][j];

    vector<vector<long long>> ret(n, vector<long long>(m));

    for (long long i = 0; i < n; i++)
        for (long long j = 0; j < m; j++)
            ret[i][j] = row[i] + col[j] - arr[i][j];

    for (long long i = 0; i < ret.size(); i++)
    {
        for (long long j = 0; j < ret[0].size(); j++)
            printf("%lld ", ret[i][j]);

        cout << endl;
    }

    return 0;
}