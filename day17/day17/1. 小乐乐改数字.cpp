#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    long long n = 0;
    cin >> n;

    string ret;

    while (n)
    {
        long long tmp = n % 10;
        n /= 10;

        if (0 == tmp % 2)
            ret += '0';
        else if (1 == tmp % 2)
            ret += '1';
    }

    // ����ǰ�� 0
    while (ret.size() && '0' == ret.back())
		ret.pop_back();

    if (0 == ret.size())
    {
        cout << 0 << endl;
    }
    else
    {
        for (int i = ret.size() - 1; i >= 0; i--)
            cout << ret[i];
        cout << endl;
    }

    return 0;
}