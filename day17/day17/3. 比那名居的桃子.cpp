#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <climits>
#include <iostream>

using namespace std;

int main()
{
    long long n = 0, k = 0;
    cin >> n >> k;

    // 录入每天能获得的快乐值
    vector<long long> happy(n + 1);
    for (long long i = 1; i <= n; i++)
        cin >> happy[i];

    // 录入每天能获得的羞耻值
    vector<long long> shame(n + 1);
    for (long long j = 1; j <= n; j++)
        cin >> shame[j];

    // 求出 快乐值 和 羞耻值 得前缀和数组
    vector<long long> sum_happy(n + 1);
    vector<long long> sum_shame(n + 1);
    for (long long i = 1; i <= n; i++)
    {
        sum_happy[i] = happy[i] + sum_happy[i - 1];
        sum_shame[i] = shame[i] + sum_shame[i - 1];
    }

    long long start = 0;
    long long hmax = LLONG_MIN;     // 求最大得快乐值 
    long long smin = LLONG_MAX;     // 求最小得羞耻值

    for (long long i = 1; i + k - 1 <= n; i++)
    {
        long long h = sum_happy[i + k - 1] - sum_happy[i - 1];
        long long s = sum_shame[i + k - 1] - sum_shame[i - 1];

        if (h > hmax)               // 取快乐值较大的，更新这天开始能获得的快乐值和羞耻值
        {
            hmax = h;
            smin = s;
            start = i;
        }
        else if (hmax == h)         // 快乐值相等时
        {
            if (s < smin)           // 取羞耻值较小的，更新这天开始能获得的羞耻值
            {
                smin = s;
                start = i;
            }
        }
    }

    cout << start << endl;

    return 0;
}