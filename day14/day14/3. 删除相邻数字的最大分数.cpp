#define _CRT_SECURE_NO_WARNINGS

// 解题思路：dp

// 1.预处理：先求每个数出现的总和，在哈希表中，选择一些不相邻的数，让他的总和最大

// 2.动态规划：当选了某一位的数之后，其后一位的数就不能再选了
//	1.状态表示：
//		f[i]：表示选到 i 位置的时候，i 位置的元素必选，此时的最大分数
//		g[i]：表示选到 i 位置的时候，i 位置的元素不选，此时的最大分数
//	2. 状态转移方程：
//		f[i] = hash[i] + g[i - 1];
//		g[i] = max(f[i - 1], g[i - 1]);

#include <map>
#include <vector>
#include <iostream>

using namespace std;

const int N = 1e4 + 10;

int main()
{
	long long n = 0;
	cin >> n;

	vector<long long> hash(N);

	for (size_t i = 0; i < n; i++)
	{
		int val = 0;
		cin >> val;
		hash[val] += val;
	}

	vector<long long> f(N);
	vector<long long> g(N);

	for (size_t i = 1; i < N; i++)
	{
		f[i] = hash[i] + g[i - 1];
		g[i] = max(f[i - 1], g[i - 1]);
	}

	cout << max(f[N - 1], g[N - 1]) << endl;

	return 0;
}