#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <vector>
#include <iostream>

using namespace std;

int main()
{
    string str1;
    string str2;
    int flag = 1;
    vector<int> hash(26);

    cin >> str1;
    cin >> str2;

    for (auto ch : str2)
        hash[ch - 'A']++;

    for (size_t i = 0; i < str1.size(); i++)
    {
        if (hash[str1[i] - 'A'] > 0)
            hash[str1[i] - 'A']--;
    }

    for (size_t i = 0; i < hash.size(); i++)
        if (hash[i] != 0)
            flag = 0;

    if (flag)
        cout << "Yes" << endl;
    else
        cout << "No" << endl;

    return 0;
}