#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    long long n = 0;
    cin >> n;

    vector<long long> arr(3 * n);
    vector<long long> ret;

    for (long long i = 0; i < 3 * n; i++)
        cin >> arr[i];

    sort(arr.begin(), arr.end());

    for (long long i = arr.size() - 2; i >= 0; i -= 2)
    {
        if (0 == n--)
            break;

        ret.push_back(arr[i]);
    }

    long long sum = 0;

    for (auto val : ret)
        sum += val;

    cout << sum << endl;

    return 0;
}