#define _CRT_SECURE_NO_WARNINGS

// 解题思路：动态规划 - 两个数组 / 两个字符串之间的 dp 问题

// 选择 s1 的 0 ~ i 区间，选择 s2 的 0 ~ j，逐渐递推到选择全部的区间为止

// 1.状态表示
//	dp[i][j] 表示 s1 中 [0, i] 区间以及 s2 中 [0, j] 区间中所有的子序列里面，最长公共子序列的长度

// 2.状态转移方程
//	a.s1[i] == s2[j]：
//		1.从 s1 的 [0, i - 1] 和 s2 的 [0, j - 1] 中挑一个最长公共子序列，再拼上 i 和 j 各自指向的字符
//		2.dp[i][j] = dp[i - 1][j - 1] + 1;
//	b.s1[i] != s2[j]：
//		1.不能再选 i 和 j 各自指向的字符了，
//			a.从 S1 的 [0, i] 区间和 s2 的 [0, j - 1] 区间，挑一个最长公共子序列，代表整体的最长公共子序列
//			b.从 s1 的 [0, i - 1] 区间和 s2 的 [0, j] 区间，挑一个最长公共子序列，代表整体的最长公共子序列
//		2.dp[i][j] = max(dp[i][j - 1], dp[i - 1][j]);

// 3.初始化
//	a.多开一行和一列，将多开的一行一列初始化成 0
//	b.dp[0][j] / dp[i][0] = 0; 

// 4.填表顺序
//	a.自伤而下，自左而右

// 5.返回值
//	a.dp[n][m];

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int n = 0, m = 0;
    string s1, s2;

    cin >> n >> m;
    cin >> s1;
    cin >> s2;

    // 1.创建 dp 表
    vector<vector<int>> dp(n + 1, vector<int>(m + 1));

    // 2.初始化 dp 表，将多出的一行一列初始化成 0

    // 3.填 dp 表
    for (size_t i = 1; i <= n; i++)
    {
        for (size_t j = 1; j <= m; j++)
        {
            if (s1[i - 1] == s2[j - 1]) // 注意 s1 和 s2 的下标映射关系
                dp[i][j] = dp[i - 1][j - 1] + 1;
            else
                dp[i][j] = max(dp[i][j - 1], dp[i - 1][j]);
        }
    }

    // 4.确定返回值
    cout << dp[n][m] << endl;

    return 0;
}