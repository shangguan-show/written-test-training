#define _CRT_SECURE_NO_WARNINGS

// 成立：素数 * 非素数 = 素数
// 不成立：素数 * 素数 = 非素数，非素数 * 非素数 = 非素数

// 即查看 a 和 b 是不是一个素数和一个非素数即可

#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

long long t;
long long a, b;

static bool is_prime(long long n)
{
    if (0 == n || 1 == n)
        return false;

    for (size_t i = 2; i <= sqrt(n); i++)
        if (0 == n % i)
            return false;

    return true;
}

int main()
{
    cin >> t;

    while (t--)
    {
        cin >> a >> b;

        bool x = is_prime(a);
        bool y = is_prime(b);

        if ((x && !y) || (!x && y)) // 素数 * 非素数 = 素数
            cout << "YES" << endl;
        else                        // 只要不是 1 素 1 非素，相乘之后都是非素数
            cout << "NO" << endl;
    }

    return 0;
}