#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

long long n, k;

int main()
{
    cin >> n >> k;

    vector<long long> arr(n);

    for (size_t i = 0; i < n; i++)
        cin >> arr[i];

    sort(arr.begin(), arr.end());

    int count = 0;
    int left = 0, right = 0;

    for (; right < n; right++)
    {
        // �˴���
        while (arr[right] - arr[left] > k && left < right)
            left++;

        count = max(count, right - left + 1);
    }

    cout << count << endl;

    return 0;
}