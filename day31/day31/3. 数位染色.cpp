#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>

using namespace std;

long long x, sum;

int main() 
{
    cin >> x;
    vector<long long> arr;

    while (x)
    {
        arr.push_back(x % 10);
        sum += arr.back();
        x /= 10;
    }

    if (1 == sum % 2)   // 如果和为奇数，则不可能有正好等于其一半的数
    {
        cout << "No" << endl;
    }
    else
    {
        int n = arr.size();

        // 1.创建 dp 表
        vector<vector<int>> dp(n + 1, vector<int>(sum * 2, false));

        // 2.初始化 dp 表
        dp[0][0] = true;

        // 3.填 dp 表
        for (size_t i = 1; i <= n; i++)
        {
            for (size_t j = 0; j <= sum / 2; j++)
            {
                dp[i][j] = dp[i - 1][j];

                if (j >= arr[i - 1])
                    dp[i][j] = dp[i - 1][j - arr[i - 1]] || dp[i - 1][j];
            }
        }

        // 4.确定返回值
        if (dp[n][sum / 2])
            cout << "Yes" << endl;
        else
            cout << "No" << endl;
    }

    return 0;
}