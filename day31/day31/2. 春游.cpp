#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

long long t, n, a, b;

int main()
{
    cin >> t;

    while (t--)
    {
        cin >> n >> a >> b;

        long long price = 0;

        if (n <= 2)
        {
            cout << min(a, b) << endl;
            continue;
        }

        if (a * 3 < b * 2)  // 组双人船
        {
            price += n / 2 * a;
            n %= 2;

            if (1 == n)
                price += min(min(a, b), b - a);
        }
        else                // 组三人船
        {
            price += n / 3 * b;
            n %= 3;

            if (1 == n)
                price += min(min(a, b), 2 * a - b);
            else if (2 == n)
                price += min(min(a, b), 3 * a - b);
        }

        cout << price << endl;
    }

    return 0;
}