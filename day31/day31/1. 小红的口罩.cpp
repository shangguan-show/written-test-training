#define _CRT_SECURE_NO_WARNINGS

#include <queue>
#include <vector>
#include <iostream>

using namespace std;

long long n, k, val, day;

// 使用小根堆，每次将 k 减去堆顶元素，
// 然后将堆顶元素 * 2 再插入进小根堆中，直到 k <= 0 为止

int main()
{
    cin >> n >> k;
    priority_queue<long long, vector<long long>, greater<long long>> pq;

    for (size_t i = 0; i < n; i++)
    {
        cin >> val;
        pq.push(val);
    }

    while (k > 0)
    {
        long long tmp = pq.top();
        pq.pop();
        k -= tmp;
        day++;
        tmp *= 2;
        pq.push(tmp);
    }

    cout << day - 1 << endl;

    return 0;
}