#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int maxLength(vector<int>& arr)
    {
        int len = 0, left = 0, right = 0;
        map<int, int> M;

        for (; right < arr.size(); right++)
        {
            M[arr[right]]++;

            while (M[arr[right]] > 1)
                M[arr[left++]]--;

            len = max(len, right - left + 1);
        }

        return len;
    }
};