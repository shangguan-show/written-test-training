#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

using namespace std;

// 初始为 0 星

int main()
{
    int t = 0;
    cin >> t;

    while (t--)
    {
        long long ret = 0;    // 统计星星数量
        long long count = 0;  // 统计连胜场次
        long long n = 0, k = 0;
        cin >> n >> k;

        for (size_t i = 0; i < n; i++)
        {
            char ch;
            cin >> ch;

            if ('W' == ch)
            {
                count++;

                if (count >= 3)
                    ret += k;
                else
                    ret++;
            }
            else if ('L' == ch)
            {
                count = 0;
                ret--;
            }
        }

        cout << ret << endl;
    }

    return 0;
}