#define _CRT_SECURE_NO_WARNINGS

// 解题思路: 贪心

// 策略：如何重排？
// 1.每次处理一批相同的字符
// 2.优先处理出现次数最多的字符，该字符可能会影响到字符的重排
// 3.将出现次数最多的字符，每隔一个位置摆一个该字符，然后处理第二多的字符，以此类推
// 4.当将下标为偶数的为止全部摆上字符之后，奇数下标都是不相邻的，剩余字符可以随意摆放

// 考虑：能不能重排？
// 1.根据次数最多的字符个数来决定能不能重排
// 2.次数最多的字符个数 > (n + 1) / 2 时，该字符必定出现相邻，无法重排，反之则能重排

#include <map>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

const int N 1e5 + 10;
char str[N];
char ret[N];
int n;

int main()
{
	cin >> n >> str;

	char max_char = 0;		// 标记出现次数最多的字符
	int max_count = 0;		// 统计出现次数最多的字符个数
	int hash[26] = { 0 };	// 统计每个字符出现的次数

	// 寻找出现次数最多的字符
	for (size_t i = 0; i < n; i++)
	{
		int index = str[i] - 'a';

		if (++hash[index] > max_count)
		{
			max_char = str[i];
			max_count = hash[index];
		}
	}

	// 判断是否能够重排
	if (max_count > (n + 1) / 2)
	{
		cout << "no" << endl;
	}
	else
	{
		cout << "yes" << endl;

		int i = 0;

		while (max_count--)				// 处理出现次数最多的字符
		{
			ret[i] = max_char;
			i += 2;
		}

		for (size_t j = 0; j < 26; i++)	// 处理剩下的字符
		{
			if (hash[j] != 0 && j + 'a' != max_char)
			{
				while (hash[j]--)
				{
					if (i >= n)
						i = 1;

					ret[i] = j + 'a';
					i += 2;
				}
			}
		}

		// 输出重排后的字符串
		for (size_t i = 0; i < n; i++)
			cout << ret[i];
		cout << endl;
	}

	return 0;
}