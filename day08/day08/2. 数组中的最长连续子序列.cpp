#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int MLS(vector<int>& arr)
    {
        if (arr.empty())
            return 0;

        vector<int> ret;

        sort(arr.begin(), arr.end());
        ret.push_back(arr[0]);

        long long len = 0;

        for (size_t i = 1; i < arr.size();)
        {
            if (arr[i] - 1 == ret.back())   // 当前元素 - 1 = ret 的最后一个元素 
            {
                ret.push_back(arr[i++]);
            }
            else if (arr[i] == ret.back())  // 当前元素 = ret 的最后一个元素，跳过
            {
                i++;
            }
            else                            // 更新长度，清空临时数组，再将当前值入数组
            {
                if (len < ret.size())
                    len = ret.size();

                ret.clear();
                ret.push_back(arr[i++]);
            }
        }

        return len > ret.size() ? len : ret.size();
    }
};