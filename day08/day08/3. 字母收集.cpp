#define _CRT_SECURE_NO_WARNINGS

// 解法：动态规划

// 1. 状态表示：dp[i][j] 表示到达 [i, j] 位置时，最大的分数是多少
// 2. 状态转移方程：dp[i][j] = max(dp[i][j - 1] + t, dp[i - 1][j] + t); (t 为走到 [i, j] 时的分数)
// 3. 如何求 t：字母为 l 时 t = 4, 为 o 时 t = 3，为 v 时 t = 2，为 e 时 t = 1，其余情况 t = 0

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

using std::max;
using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::string;

int n = 0, m = 0;
const int N = 510;
int dp[N][N] = { 0 };
char graph[N][N] = { 0 };

int main()
{
	cin >> n >> m;

	for (size_t i = 1; i <= n; i++)
		for (size_t j = 1; j <= m; j++)
			cin >> graph[i][j];

	for (size_t i = 1; i <= n; i++)
	{
		for (size_t j = 1; j <= m; j++)
		{
			int t = 0;	// 当前位置的分数
			
			switch (graph[i][j])
			{
			case 'l':t = 4; break;
			case 'o':t = 3; break;
			case 'v':t = 2; break;
			case 'e':t = 1; break;
			}

			dp[i][j] = max(dp[i][j - 1] + t, dp[i - 1][j] + t);
		}
	}

	cout << dp[n][m] << endl;

	return 0;
}