#define _CRT_SECURE_NO_WARNINGS

#include <climits>
#include <algorithm>

using namespace std;

// 解题思路：dfs / 递归 / 树形 dp

// 树形 dp：拿取左右子树的信息，然后在根结点 (当前层) 进行整合，再将整合好的信息向上返回
// 
//  a.需要左子树提供的信息：以左子树为根的最大单链和 (不是路径和)，如果和为负数，则提供 0
//  b.需要右子树提供的信息：以右子树为根的最大单链和 (不是路径和)，如果和为负数，则提供 0
//  c.根节点需要整合的信息：经过根结点的最大路径和，当前结点的值 + 左右子树的最大单链和
//  d.向上需要返回什么信息：以当前子树为根的最大单链和，有以下三种情况：
//      1.当前根结点的值 
//      2.当前根节点的值 + 左子树的最大单链和 
//      3.当前根节点的值 + 右子树的最大单链和

 struct TreeNode 
 {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
    
    TreeNode(int x) 
        : val(x), left(nullptr), right(nullptr) 
    {}
 };

 class Solution
 {
 private:
     int ret = INT_MIN;
 public:
     int dfs(TreeNode* root)
     {
         if (nullptr == root)
             return 0;

         int left = max(dfs(root->left), 0);         // 求以左子树为根的最大单链和
         int right = max(dfs(root->right), 0);       // 求以右子树为根的最大单链和
         ret = max(root->val + left + right, ret);   // 计算经过根节点的最大路径和

         return root->val + max(left, right);        // 返回以当前子树为根的最大单链和
     }

     int maxPathSum(TreeNode* root)
     {
         dfs(root);

         return ret;
     }
 };