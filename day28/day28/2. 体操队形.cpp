#define _CRT_SECURE_NO_WARNINGS

// 解题思路：dfs / 递归

// 提供一个位置，递归到某一层时在这个位置枚举剩余的所有数是否适合放在该位置

#include <vector>
#include <iostream>

using namespace std;

const int N = 15;
bool visit[N];
int arr[N];
int ret;
int n;

static void dfs(int pos)
{
	if (n + 1 == pos)		// 找到一种合法的方案
	{
		ret++;
		return;
	}

	for (size_t i = 1; i <= n; i++)
	{
		if (visit[i])		// i 位置的值被使用过，直接下一个数
			continue;

		if (visit[arr[i]])	// 队员要求的位置被使用了，之后肯定不合法
			return;

		visit[i] = true;	// 放上 i 号队员
		dfs(pos + 1);		// 填下一号位置
		visit[i] = false;	// 还原现场
	}
}

int main()
{
	cin >> n;
	for (size_t i = 1; i <= n; i++)
		cin >> arr[i];

	dfs(1);

	cout << ret << endl;

	return 0;
}