#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

long long fun(long long x)
{
    if (0 == x % 2)
        return x;

    if (x < 20 && x > 10)
        return -1;

    vector<int> arr;

    while (x)
    {
        arr.push_back(x % 10);
        x /= 10;
    }

    for (size_t i = 0; i < arr.size(); i++)
    {
        if (0 == arr[i] % 2)
        {
            swap(arr[0], arr[i]);
            break;
        }
    }

    if (0 != arr[0] % 2)
        return -1;

    reverse(arr.begin(), arr.end());

    long long ret = arr[0];

    for (size_t i = 1; i < arr.size(); i++)
        ret = ret * 10 + arr[i];

    return ret;
}

int main()
{
    long long q = 0;
    cin >> q;

    while (q--)
    {
        long long x = 0;
        cin >> x;

        cout << fun(x) << endl;
    }

    return 0;
}