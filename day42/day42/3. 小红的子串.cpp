// 解题思路：滑动窗口 + 前缀和(思想)

//	1.先求字符种类在 [1, L - 1]	中的子串数量 x
//	2.再求字符种类再 [1, R]		中的子串数量 y
//	3.最终结果是 y - x

#include <vector>
#include <string>
#include <iostream>

using namespace std;

int n, l, r;
string str;

// 找出字符种类在 1 ~ val 中的子串数量 
static long long find(int val)
{
	if (0 == val)
		return 0;

	// 滑动窗口
	int left = 0, right = 0;
	int hash[26] = { 0 };	// 统计每种字符出现的次数
	int kinds = 0;			// 表示窗口中字符的种类数
	long long ret = 0;		// 统计最终子串的个数

	for (; right < n; right++)
	{
		// 窗口中将要多出一种新的字符
		if (0 == hash[str[right] - 'a']++)
			kinds++;

		// 窗口中字符的种类数超过规定的右区间，退窗口
		while (kinds > val)
		{
			// 当前退窗口的字符为该种字符的最后一个，字符种类数 - 1
			if (1 == hash[str[left] - 'a']--)
				kinds--;
			left++;
		}

		ret += right - left + 1;
	}

	return ret;
}

int main()
{
	cin >> n >> l >> r;
	cin >> str;

	// y - x
	cout << find(r) - find(l - 1) << endl;

	return 0;
}