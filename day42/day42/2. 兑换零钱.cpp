// 解题思路：dp - 完全背包

/*1.状态表示*/
//	a.dp[i][j] 表示：从前 i 个纸币中挑选，总和正好为 j，此时所有的最少纸币的张数

/*2.状态转移方程*/
//	a.挑选 n 个 i 处的纸币：
//		1.根据挑选的 i 处的纸币数量决定去前面挑几张纸币
//		2.dp[i][j] = dp[i][j - arr[i] + 1
//	b.挑选 0 个 i 处的纸币：
//		1.去 1 ~ i - 1 中凑总和为 j 的纸币数
//		2.dp[i][j] = dp[i - 1][j]
//	c.选择上述两种情况的最小值：dp[i][j] = min(dp[i - 1][j], dp[i][j - arr[i]] + 1);

/*3.初始化*/
//	a.dp[0][0] = 0; 没有钱时能凑的数量为 0
//	b.第一行其余格子为 +∞，没钱的时候不能挑

/*4.填表顺序*/
//	a.自上而下，自左而右

/*5.返回值*/
//	a.do[n][aim]

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	int n = 0;	// 数组 arr 的长度
	int aim = 0;// 要找的钱数

	cin >> n >> aim;

	vector<int> arr(10001);
	for (int i = 1; i <= n; i++)
		cin >> arr[i];

	vector<int> dp(10001, 0x3f3f3f3f);
	dp[0] = 0;

	for (int i = 1; i <= n; i++)
		for (int j = arr[i]; j <= aim; j++)
			dp[j] = min(dp[j], dp[j - arr[i]] + 1);

	if (dp[aim] >= 0x3f3f3f3f)
		cout << -1 << endl;
	else
		cout << dp[aim] << endl;

	return 0;
}