/* 贪心 + 模拟 */

// 固定 arr[i] 为第 2 个数，则求出前 i 个数中的最小值 a，再用 arr[i] - a 即可

#include <vector>
#include <climits>
#include <iostream>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int getDis(vector<int>& A, int n)
    {
        int MIN = A[0];
        int ret = INT_MIN;

        for (size_t i = 1; i < n; i++)
        {
            MIN = min(MIN, A[i]);
            ret = max(ret, A[i] - MIN);
        }

        return ret;
    }
};