#define _CRT_SECURE_NO_WARNINGS

// 解题思路：动态规划 - 01背包

// 1.状态表示
//	dp[i][j] 表示，从前 i 个物品中挑选，总体积不超过 j 时，此时的最大体积是多少

// 2.状态转移方程
//	a.挑选 i 这个位置的物品：在 0 ~ i-1 中挑不超过 j 的最大体积
//		dp[i - 1][j]
//	b.不挑 i 这个位置的物品：在 0 ~i-1 中挑不超过 j - arr[i] 的最大体积
//		dp[i - 1][j - arr[i]] + arr[i]
//	c.要这两种情况的最大值
//		dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - arr[i]] + arr[i]);
//		前提：j >= arr[i]

// 3.初始化
//	

// 4.填表顺序
//	从上往下，从左往右

// 5.返回值
//	V - dp[n][v]

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

const int N = 35, M = 2e4 + 10;

int n, v;

int main()
{
	cin >> v >> n;

	vector<int> arr(N);
	vector<vector<int>> dp(N, vector<int>(M);

	for (size_t i = 1; i <= n; i++)
		cin >> arr[i];
	
	for (size_t i = 1; i <= n; i++)
	{
		for (size_t j = 0; j <= v; j++)
		{
			dp[i][j] = dp[i - 1][j];

			if (j >= arr[i])
				dp[i][j] = max(dp[i][j], dp[i - 1][j - arr[i]] + arr[i]);
		}
	}

	cout << (v - dp[n][v]) << endl;

	return 0;
}