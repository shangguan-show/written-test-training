#define _CRT_SECURE_NO_WARNINGS

// 找出数组中较大的数，如果较小的数不停的 * 2 最终能和该较大的数相等，则输出 YES。
// 较大的数不需要去 * 2，因为本来就是较小的数通过不停的 * 2 得出的，无非就是多 * 几个 2 而已
// 如果较小的数无法变成较大的数，那么较大的数不管怎么 * 2 都不行

// 如果较小的数 可以 变成较大的数：较大的数不用改变
// 如果较小的数 不能 变成较大的数：较大的数变也白变

// 找出数组中的最大值 MAX，看看数组中的其他数不停的 * 2 是否能变成 MAX

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

static bool is_equal(vector<long long>& arr, long long MAX)
{
	for (size_t i = 0; i < arr.size(); i++)
	{
		if (0 != MAX % arr[i])
			return false;

		int x = MAX / arr[i];

		if (0 != (x - (x & -x)))
			return false;
	}

	return true;
}

int main()
{
	int n = 0;
	cin >> n;

	long long MAX = 0;
	vector<long long> arr(n);
		
	for (size_t i = 0; i < n; i++)
	{
		cin >> arr[i];
		MAX = max(MAX, arr[i]);
	}

	if (is_equal(arr, MAX))
		cout << "YES" << endl;
	else
		cout << "NO" << endl;

	return 0;
}