#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	string strA, strB;
	cin >> strA >> strB;

	// 统计最少的不相同字符数
	int ret = strA.size();	

	// 枚举 B 串的起始位置
	for (size_t i = 0; i <= strB.size() - strA.size(); i++)
	{
		int tmp = 0;

		for (size_t j = 0; j < strA.size(); j++)
			if (strB[i + j] != strA[j])
				tmp++;
		
		ret = min(tmp, ret);
	}

	cout << ret << endl;

	return 0;
}