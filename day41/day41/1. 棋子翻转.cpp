#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

class Solution
{
private:
    int dx[4] = { 0, 0, 1, -1 };
    int dy[4] = { 1, -1, 0, 0 };
public:
    vector<vector<int>> flipChess(vector<vector<int>>& A, vector<vector<int>>& f)
    {
        for (int i = 0; i < f.size(); i++)
        {
            int a = f[i][0] - 1;
            int b = f[i][1] - 1;

            for (size_t k = 0; k < 4; k++)
            {
                int x = a + dx[k];
                int y = b + dy[k];

                if (x >= 0 && x < 4 && y >= 0 && y < 4)
                {
                    if (1 == A[x][y])
                        A[x][y] = 0;
                    else if (0 == A[x][y])
                        A[x][y] = 1;
                }
            }
        }

        return A;
    }
};