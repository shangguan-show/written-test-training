#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int n = 0;
    cin >> n;

    vector<long long> arr(n + 1);
    for (size_t i = 1; i <= n; i++)
        cin >> arr[i];

    vector<long long> dp(n + 1);

    for (size_t i = 3; i <= n; i++)
        dp[i] = max(dp[i - 3] + arr[i - 1], dp[i - 1]);

    cout << dp[n] << endl;

    return 0;
}