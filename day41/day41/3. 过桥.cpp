// 解题思路：贪心 + bfs，图论 - 找最短路

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

static int bfs(int n, vector<int>& arr)
{
	int ret = 0;
	int left = 1, right = 1;
	
	while (left <= right)
	{
		ret++;			// 向后跳一步
		int r = right;	// 新的右区间

		for (int i = left; i <= right; i++)
		{
			r = max(r, arr[i] + i);

			if (r >= n)	// 已经跳到最右端点
				return ret;
		}

		left = right + 1;
		right = r;
	}

	return -1;
}

int main()
{
	int n = 0;
	cin >> n;

	vector<int> arr(n);
	for (size_t i = 0; i < n; i++)
		cin >> arr[i];

	cout << bfs(n, arr) << endl;

	return 0;
}