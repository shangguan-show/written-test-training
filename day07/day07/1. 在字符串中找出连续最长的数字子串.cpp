#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    string str;
    cin >> str;

    int index = 0;  // 记录最长的数字子串的下标
    int length = 0; // 记录数字子串的长度
    int left = 0, right = 0;

    // for (size_t i = 0; i < str.size(); i++)
    while (left < str.size())
    {
        if (isdigit(str[right]))
        {
            right++;
        }
        else
        {
            if (right - left > length)
            {
                index = left;
                length = right - left;
            }

            left = right;
            right++;
        }
    }

    for (size_t i = index + 1; i < index + length; i++)
        cout << str[i];
    cout << endl;

    return 0;
}