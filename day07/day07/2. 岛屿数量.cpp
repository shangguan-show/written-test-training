#define _CRT_SECURE_NO_WARNINGS

#include <vector>

using namespace std;

class Solution
{
private:
    int count = 0;
    int n = 0, m = 0;			    // 记录矩阵的大小
    int dx[4] = { 0, 0, 1, -1 };    // 标记 x 的四个方向
    int dy[4] = { 1, -1, 0, 0 };    // 标记 y 的四个方向
public:
    void dfs(vector<vector<char>>& grid, int x, int y)
    {
        grid[x][y] = '0';

        for (int i = 0; i < 4; i++)
        {
            int a = x + dx[i];		// 新的 x 坐标
            int b = y + dy[i];		// 新的 y 坐标

            // 新坐标处于合法位置，且新坐标为字符 1
            if (a >= 0 && a < n && b >= 0 && b < m && grid[a][b] == '1')
                dfs(grid, a, b);
        }
    }

    int solve(vector<vector<char>>& grid)
    {
        n = grid.size();
        m = grid[0].size();

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if ('1' == grid[i][j])  // 找到一个 1 就让岛屿数量++
                {
                    count++;
                    dfs(grid, i, j);    // 将与该 1 相邻的 1 都置为 0，因为相邻的算作一个岛屿
                }
            }
        }

        return count;
    }
};