#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>

using namespace std;

bool isok(int a, int b, int c)
{
    return (a + b > c) && (a + c > b) && (b + c > a);
}

int main()
{
    int t = 0;
    cin >> t;

    while (t--)
    {
        bool flag = false;
        vector<long long> arr(6);

        for (size_t i = 0; i < 6; i++)
            cin >> arr[i];

        for (size_t i = 0; i < 4; i++)
        {
            for (size_t j = i + 1; j < 5; j++)
            {
                for (size_t k = j + 1; k < 6; k++)
                {
                    int tmp[3] = { 0 };   // 存储与 i j k 不同位置的另外三个元素的位置

                    for (size_t index = 0, a = 0; a < 6; a++)
                        if (a != i && a != j && a != k)
                            tmp[index++] = a;

                    bool ret1 = isok(arr[i], arr[j], arr[k]);
                    bool ret2 = isok(arr[tmp[0]], arr[tmp[1]], arr[tmp[2]]);

                    if (ret1 && ret2)
                        flag = true;
                }
            }
        }

        if (flag)
            cout << "Yes" << endl;
        else
            cout << "No" << endl;
    }

    return 0;
}