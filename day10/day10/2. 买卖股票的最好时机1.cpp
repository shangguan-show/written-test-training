#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>

using namespace std;

int main()
{
    int n = 0;
    cin >> n;
    vector<int> prices(n);

    for (size_t i = 0; i < n; i++)
        cin >> prices[i];

    int buyi = 0;   // 买入价格的下标                            
    int profit = 0; // 记录利润

    for (size_t i = 0; i < n; i++)
    {
        if (prices[buyi] > prices[i])            // 更新最小买入价格的下标
            buyi = i;

        int ret = prices[i] - prices[buyi];     // 最小买入价格和当前值做差

        if (ret > profit)                       // 差值 > 之前的利润时更新利润  
            profit = ret;
    }

    cout << profit << endl;

    return 0;
}