#define _CRT_SECURE_NO_WARNINGS

// 解法：动态规划 - 路径类

// 1. 状态表示：
//		dp[i][j] 表示：从 [0, 0] 位置出发，到达 [i, j] 位置，一共有多少种方法

// 2. 状态转移方程：
//		到达 [i, j] 的路径数 = 到达 [i - 1, j] 和 [i, j - 1] 的路径数之和
//		如果能够走到 [i, j] (马 不能 控制该位置) dp[i][j] = dp[i - 1][j] + dp[i][j - 1]
//		如果不能走到 [i, j] (马 能够 控制该位置) dp[i][j] = 0;

// 3. 初始化：dp 表规模 dp[n + 2][m + 2]，dp[0][1] = 1 或 dp[1][0] = 1

#include <cmath>
#include <vector>
#include <iostream>

using std::cin;
using std::endl;
using std::cout;
using std::vector;


int main()
{
	size_t n = 0, m = 0, x = 0, y = 0;
	cin >> n >> m >> x >> y;

	long long dp[25][25] = { 0 };

	dp[0][1] = 1;
	x += 1, y += 1;

	for (size_t i = 1; i <= n + 1; i++)
	{
		for (size_t j = 1; j <= m + 1; j++)
		{
			
			if ((i != x && j != y && 3 == abs(i - x) + abs(j - y)) || (i == x && j == y))
				dp[i][j] = 0;	// 马能控制的位置置为 0 
			else
				dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
		}
	}

	cout << dp[n + 1][m + 1] << endl;

	return 0;
}