#define _CRT_SECURE_NO_WARNINGS

#include <string>

using std::string;

class Solution
{
public:
    int getLongestPalindrome(string str)
    {
        int length = 1;

        for (int i = 0; i < str.size(); i++)
        {
            for (int j = i + 1; j < str.size(); j++)
            {
                int left = i, right = j;

                while (left < right)
                {
                    if (str[left] != str[right])
                        break;

                    left++;
                    right--;
                }

                if (left >= right && length < j - i + 1)
                    length = j - i + 1;
            }
        }

        return length;
    }
};