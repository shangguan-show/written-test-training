#define _CRT_SECURE_NO_WARNINGS

// 解题思路：数学 - 排列组合

// n 种气球，m 个位置
// 第一个位置，有 n 种放法，之后的 m - 1 个位置因为不能出现重复，因此都只有 n - 1 种摆法
// n * m - 1 次 (n - 1) 即可

#include <iostream>

using namespace std;

int main()
{
    int n = 0, m = 0;
    cin >> n >> m;

    long long ret = n;

    for (size_t i = 1; i < m; i++)  // 做 m - 1 次 n * (n - 1)
        ret = (ret * (n - 1)) % 109;

    if (1 == n && 1 != m)           // 如果只有 1 种气球，摆放位置 m 却不是 1 个，摆法为 0
        ret = 0;

    cout << ret << endl;

    return 0;
}