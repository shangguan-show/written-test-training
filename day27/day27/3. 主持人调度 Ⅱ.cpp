#define _CRT_SECURE_NO_WARNINGS

#include <queue>
#include <vector>
#include <algorithm>

using namespace std;

// 解法：
// 1.将区间按照左端点 (活动开始时间) 排序
// 2.按照顺序依次处理没有分配的区间，
//  然后看看该区间能够放在已处理好的区间的后面即可

// 优化：
// 1.将已经处理好的区间放进小根堆中，将剩下的区间的最小开始时间和堆顶的结束时间比较
//  2.如果连最小的结束时间都不能满足的话，这个活动区间就只能重新弄一个主持人去主持

class Solution
{
public:
    int minmumNumberOfHost(int n, vector<vector<int>>& startEnd)
    {
        // 按照左端点排序
        sort(startEnd.begin(), startEnd.end());

        // 创建一个小根堆
        priority_queue<int, vector<int>, greater<int>> heap;

        // 首先要有一个人主持活动，先将第一个区间的右端点的值放入堆中
        heap.push(startEnd[0][1]);

        // 处理剩下的区间
        for (size_t i = 1; i < n; i++)
        {
            int a = startEnd[i][0];
            int b = startEnd[i][1];

            
            if(a >= heap.top()) // 没有重叠
            {
                heap.pop();     // 将之前的区间删掉
                heap.push(b);   // 将合适的区间的右端点进入已经处理好的区间
            }
            else                // 有重叠
            {
                heap.push(b);   // 重新安排一个人主持活动
            }
        }

        return heap.size();
    }
};