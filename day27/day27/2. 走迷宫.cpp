#define _CRT_SECURE_NO_WARNINGS

// 解题思路：bfs

#include <queue>
#include <vector>
#include <string>
#include <cstring>
#include <iostream>

using namespace std;

int dx[4] = { 0,0,1,-1 };
int dy[4] = { 1,-1,0,0 };
const int N = 1010;
int x1, y1, x2, y2;					// 标记起始位置和终止位置
char arr[N][N];
int vis[N][N];						// 标记 [i,j] 位置是否搜索过，以及到 [i,j] 时的最短距离
int n, m;

static int bfs()
{
	if ('*' == arr[x2][y2])			// 给的坐标不合法
		return -1;

	memset(vis, -1, sizeof(vis));	// 初始化成 -1，表示未搜索过

	queue<pair<int, int>> q;		// 队列中的每个元素存储的是当前位置的下标
	q.push({ x1,y1 });				// 将起点入队列
	vis[x1][y1] = 0;				// 起点的最短距离为 0

	while (q.size())				// 队列不为空时执行循环
	{
		auto [a, b] = q.front();	// 从 [a,b] 处的四个方向开始搜索
		q.pop();

		for (size_t i = 0; i < 4; i++)
		{
			int x = a + dx[i];		// 获取下一个点的 x 坐标
			int y = b + dy[i];		// 获取下一个点的 y 坐标

			// 判断下一个点是否能够进入
			if ((x >= 1 && x <= n) && (y >= 0 && y <= m) &&
				('.' == arr[x][y]) && (-1 == vis[x][y]))
			{
				q.push({ x,y });
				vis[x][y] = vis[a][b] + 1;

				if (x == x2 && y == y2)
					return vis[x2][y2];
			}
		}
	}

	return -1;
}

int main()
{
	cin >> n >> m >> x1 >> y1 >> x2 >> y2;

	for (size_t i = 1; i <= n; i++)
		for (size_t j = 1; j <= m; j++)
			cin >> arr[i][j];

	cout << bfs() << endl;

	return 0;
}