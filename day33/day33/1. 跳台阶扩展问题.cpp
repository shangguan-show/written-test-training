#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <iostream>

using namespace std;

int main()
{
    int n = 0;
    cin >> n;

    cout << pow(2, n - 1) << endl;

    return 0;
}