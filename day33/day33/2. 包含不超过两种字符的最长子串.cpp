#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

int n;
string str;

int main()
{
    cin >> str;

    map<char, int> m;
    int max_length = 0;
    int left = 0, right = 0;

    for (; right < str.size(); right++)
    {
        m[str[right]]++;

        while (m.size() > 2 && left < right)
        {
            m[str[left]]--;
            if (0 == m[str[left]])
                m.erase(str[left]);
            left++;
        }

        max_length = max(max_length, right - left + 1);
    }

    cout << max_length << endl;

    return 0;
}