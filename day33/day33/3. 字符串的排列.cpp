#define _CRT_SECURE_NO_WARNINGS

#include <set>
#include <string>
#include <vector>

using namespace std;

// ȫ���� + ȥ��

class Solution
{
private:
    string path;
    bool check[10] = { 0 };
    set<string> s;
public:
    void dfs(string& str)
    {
        if (path.size() == str.size())
        {
            s.insert(path);
            return;
        }

        for (size_t i = 0; i < str.size(); i++)
        {
            if (false == check[i])
            {
                path.push_back(str[i]);
                check[i] = true;
                dfs(str);
                path.pop_back();
                check[i] = false;
            }
        }
    }

    vector<string> Permutation(string str)
    {
        dfs(str);
        vector<string> ret;

        for (auto s : s)
            ret.push_back(s);

        return ret;
    }
};