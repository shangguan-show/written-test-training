#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    long long i = 1;
    long long x = 0;
    cin >> x;

    while (pow(i, 2) < x)
        i++;

    if (pow(i, 2) - x < x - pow(i - 1, 2))
        cout << (long long)pow(i, 2) << endl;
    else
        cout << (long long)pow(i - 1, 2) << endl;

    return 0;
}