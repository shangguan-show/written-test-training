#define _CRT_SECURE_NO_WARNINGS

// 1.利用 hash 表统计每个声部的人数，然后用这些人数去分组
// 2.枚举最终的分配结果中，最多的人数 x，然后根据某声部的人数来求出总共要分出几组
//  a 声部分的组 = a 声部的人数 / 一组最多能装的人数 x  + a % x 如果有余数则多分配一个小组
//      a / x + (0 == a % x ? 0 : 1);
// 将所有声部能分的组数相加 <= m 则合法，反之非法

#include <map>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

// 暴力枚举
int main()
{
    int n = 0, m = 0;
    cin >> n >> m;

    vector<int> arr(n);
    map<int, int> hash;

    int Max = 0;        // 统计声部最多的人数

    for (size_t i = 0; i < n; i++)
    {
        cin >> arr[i];
        hash[arr[i]]++;
        Max = max(Max, hash[arr[i]]);
    }

    int flag = 1;

    for (size_t x = 1; x <= Max; x++)
    {
        int sum = 0;    // 统计分配的组数之和

        for (size_t j = 0; j < hash.size(); j++)
            sum += hash[j] / x + (0 == hash[j] % x ? 0 : 1);

        if (sum <= m)   // 所有声部能分的组数在正常状况
        {
            cout << x << endl;
            flag = 0;
            break;
        }
    }

    if (flag != 0)      // 没有在正常情况下输出内容
        cout << -1 << endl;

    return 0;
}