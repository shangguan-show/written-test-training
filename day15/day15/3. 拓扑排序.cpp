#define _CRT_SECURE_NO_WARNINGS

// 解法：拓扑排序 - BFS

// 1.建图：用数据结构将题目中的信息存储起来
// 2.统计每个点的入度，将所有入度为 0 的点加入到队列中
// 3.当队列不为空时，执行循环
//	a.拿出队头元素，并从队列中删除该元素
//	b.修改队头元素链接的点的入度信息
//	c.如果这个点链接的那个点的入度也为 0，将其加入队列

#include <queue>
#include <vector>
#include <iostream>

using namespace std;

const int N = 2e5 + 10;				// 数据范围

int main()
{
	int n = 0, m = 0;				// n 个点，m 条边
	cin >> n >> m;

	queue<int> q;					// 存储入度为 0 的点
	vector<int> in(N);				// 表示入度信息
	vector<vector<int>> edges(N);	// edges[i] 表示 i 这个点所链接的边的信息

	for (size_t i = 0; i < m; i++)
	{
		int a = 0, b = 0;
		cin >> a >> b;
		edges[a].push_back(b);		// 有一个点 a 指向 点 b 的一条边
		in[b]++;					// b 点的入度 + 1
	}

	for (size_t i = 1; i <= n; i++)	// 将入度为 0 的点入队列
		if (0 == in[i])
			q.push(i);

	vector<int> ret;				// 记录最终结果

	while (!q.empty())				// 队列不为空时
	{
		int a = q.front();			// 拿出队头元素
		ret.push_back(a);
		q.pop();

		for (auto b : edges[a])		// 修改 a 所链接的点的入度
			if (0 == --in[b])		// 修改后如果入度为 0 则入队列
				q.push(b);
	}

	if (n == ret.size())
	{
		for (size_t i = 0; i < n - 1; i++)
			cout << ret[i] << " ";
		cout << ret[n - 1];			// 防止检测到最后的空格
	}
	else
	{
		cout << -1 << endl;
	}

	return 0;
}