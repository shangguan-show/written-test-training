#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <algorithm>

using namespace std;

#include <algorithm>

class Solution
{
public:
    bool IsContinuous(vector<int>& numbers)
    {
        int index = 0;
        int count_0 = 0;
        sort(numbers.begin(), numbers.end());

        for (; index < numbers.size(); index++)                 // 跳过所有的 0，并统计 0 的数量
        {
            if (numbers[index] != 0)
                break;
            else
                count_0++;
        }

        index++;

        for (; index < numbers.size(); index++)
        {
            if (numbers[index] == numbers[index - 1])           // 出现相同的数字
            {
                return false;
            }
            else
            {
                int tmp = numbers[index] - numbers[index - 1];  // 记录前后两个数的差值

                if (tmp > 1)                                    // 差值 > 1
                {
                    if (count_0 < tmp - 1)                      // 0 的数量不够填补差值
                        return false;
                    else                                        // 0 的数量足够填补差值，去填 tmp - 1 个 0
                        count_0 -= tmp - 1;
                }
            }
        }

        return true;
    }
};