#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>

using namespace std;

int main()
{
    long long n = 0;
    cin >> n;

    string ret;
    int tmp = n, count = 1;

    while (tmp)
    {
        if (0 == count % 3)
        {
            ret += tmp % 10 + '0';
            ret += ',';
        }
        else
            ret += tmp % 10 + '0';

        count++;
        tmp /= 10;
    }

    if (',' == ret.back())
        ret.pop_back();

    for (int i = ret.size() - 1; i >= 0; i--)
        cout << ret[i];
    cout << endl;

    return 0;
}