#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

using namespace std;

int fib(int n)
{
    if (1 == n)
        return 1;
    if (2 == n)
        return 2;

    return fib(n - 1) + fib(n - 2);
}

int main()
{
    int n = 0;
    cin >> n;

    cout << fib(n) << endl;

    return 0;
}