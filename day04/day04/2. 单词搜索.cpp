#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <string>
#include <iostream>

using namespace std;

// 解题思路
// 1. 遍历字符串矩阵，只要找到一个字符和 word 字符串的第一个字符相等时，就从矩阵的该位置出发使用 dfs / bfs 进行搜索
// 2. 如果从该位置出发找到了这个单词，就返回 true，反之去寻找下一个矩阵中等于 word 首字符的字符位置执行 dfs / bfs

class Solution
{
private:
	int m = 0, n = 0;				// 记录矩阵的大小
	bool visit[101][101] = { 0 };	// 用来标记矩阵中的对应位置是否被使用过
	int dx[4] = { 0, 0, 1, -1 };	// 记录 x 的方位
	int dy[4] = { 1, -1, 0, 0 };	// 记录 y 的方位
public:
	bool dfs(vector<string>& board, int x, int y, string& word, size_t pos)
	{
		if (pos == word.size() - 1)	// 已经访问到 word 的最后一个位置
			return true;

		visit[x][y] = true;			// x y 是首次找到 word 首字符的我位置，已被搜索过
		
		for (int i = 0; i < 4; i++)
		{
			int a = x + dx[i];		// 新的 x 坐标
			int b = y + dy[i];		// 新的 y 坐标

			// 新坐标处于合法位置，且新坐标未被访问过，且新坐标的字符等于 word 的下一个字符
			if (a >= 0 && a < m && b >= 0 && b < n
				&& !visit[a][b] && board[a][b] == word[pos + 1])
				if (dfs(board, a, b, word, pos + 1))
					return true;
		}

		visit[x][y] = false;

		return false;
	}

	bool exist(vector<string>& board, string word)
	{
		m = board.size();			// 矩阵的高度
		n = board[0].size();		// 矩阵的长度

		for (int i = 0; i < m; i++)	// 寻找与 word 首字符相等的位置，然后从该处递归下去
		{
			for (int j = 0; j < n; j++)
			{
				if (board[i][j] == word[0])
				{
					if (dfs(board, i, j, word, 0))
						return true;// 搜索成功
				}
			}
		}

		return false;
	}
};