#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{

    int n = 0, a = 0, b = 1, c = 0;
    cin >> n;

    while (n > c)                       // 求到 > n 位置的 fib 数
    {
        a = b;
        b = c;                          // b 是 n 前一个 fib 数
        c = a + b;                      // c 是 n 后一个 fib 数
    }

    cout << min(c - n, n - b) << endl;  // 输出 n 与前后两个 fib 数的最小差值

    return 0;
}