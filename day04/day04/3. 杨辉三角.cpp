#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
    int n = 0;
    cin >> n;

    vector<vector<int>> arr(n);

    for (size_t i = 0; i < n; i++)
    {
        vector<int> tmp;

        for (size_t j = 0; j <= i; j++)
        {
            if (0 == j || i == j)
                tmp.push_back(1);
            else
                tmp.push_back(arr[i - 1][j - 1] + arr[i - 1][j]);
        }

        arr[i] = tmp;
    }

    for (size_t i = 0; i < n; i++)
    {
        for (size_t j = 0; j <= i; j++)
            printf("%5d", arr[i][j]);
        printf("\n");
    }

    return 0;
}