#define _CRT_SECURE_NO_WARNINGS

#include <vector>

using namespace std;

class Solution 
{
public:
    int jewelleryValue(vector<vector<int>>& frame) 
    {
        // 1.创建 dp 表
        int n = frame.size();
        int m = frame[0].size();
        vector<vector<int>> dp(n + 1, vector<int>(m + 1));

        // 2.初始化 
        // dp[1][1] 的价值 = 货架左上角物品的价值

        // 3.填 dp 表
        for (size_t i = 1; i <= n; i++)
            for (size_t j = 1; j <= m; j++)
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + frame[i- 1][j - 1];

        // 4.确定返回值
        return dp[n][m];
    }
};