#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>

using namespace std;

// 求最大公约数
static long long max_factor(long long a, long long b)
{
    while (a % b)
    {
        long long c = a % b;
        a = b;
        b = c;
    }

    return b;
}

int main()
{
    // 录入怪物数量和初始战力
    long long n = 0, x = 0;
    cin >> n >> x;

    // 录入每个怪物的战力
    vector<long long> arr(n);
    for (size_t i = 0; i < n; i++)
        cin >> arr[i];

    for (size_t i = 0; i < arr.size(); i++)
    {
        if (x >= arr[i])
            x += arr[i];
        else
            x += max_factor(x, arr[i]);
    }

    cout << x << endl;

    return 0;
}