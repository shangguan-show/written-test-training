#define _CRT_SECURE_NO_WARNINGS

// 解题思路：

// 使用判断回文字符串的方式，不过 left 和 right 分别指向的是字符串
// left 和 right 必须在各自的字符串中挑选出同样的字符，然后往中心缩，最后才能确保是回文串

// 如何判断是否能组成回文串
//  a.只需要判断 left 和 right 各自所指向的字符串中有没有同样的字符即可

// 如何判断两个字符串中有相同的字符
//  1.两层 for 循环
//  2.哈希表，如果两个哈表中有同样的字符即可

#include <vector>
#include <string>
#include <iostream>

using namespace std;


// 判断两个串中是否有相同字符
static bool is_same_char(string& left, string& right)
{
    vector<int> hash1(26);
    vector<int> hash2(26);

    // 存入哈希表
    for (auto ch : left)
        hash1[ch - 'a']++;
    for (auto ch : right)
        hash2[ch - 'a']++;

    // 判断两个哈希表中是否有相同字符
    for (auto ch : left)
        if (hash1[ch - 'a'] && hash2[ch - 'a'])
            return true;
    
    return false;
}

// 判断是否能构成回文串
static bool is_back(vector<string>& strs)
{
    int left = 0, right = strs.size() - 1;

    while (left <= right)
    {   
        // 没有相同字符时返回 false
        if (!is_same_char(strs[left], strs[right]))
            return false;
        
        left++;
        right--;
    }

    return true;
}

int main()
{
    int t = 0;
    cin >> t;

    while (t--)
    {
        int n = 0;
        cin >> n;
        vector<string> strs(n);

        for (size_t i = 0; i < n; i++)
            cin >> strs[i];

        if (is_back(strs))
            cout << "Yes" << endl;
        else
            cout << "No" << endl;
    }

    return 0;
}