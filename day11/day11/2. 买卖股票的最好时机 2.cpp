#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>

using namespace std;

int main()
{
    int n = 0;
    cin >> n;

    int profit = 0;
    vector<int> prices(n);

    for (size_t i = 0; i < n; i++)
        cin >> prices[i];

    // 在第 i 天卖出，i - 1 天买入
    // 卖出价格 - 买入价格 > 0 时差值就是这两天的利润，将小利润累加到 profit 上

    for (size_t i = 1; i < prices.size(); i++)
    {
        int price = prices[i] - prices[i - 1];

        if (price > 0)
            profit += price;
    }

    cout << profit << endl;

    return 0;
}