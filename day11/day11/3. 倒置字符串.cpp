#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    string str;
    getline(cin, str);

    int left = 0, right = 0;
    reverse(str.begin(), str.end());    // 先逆置整个字符串

    while (left < str.size())           // 再逆置每个单词
    {
        while (str[right] != ' ' && right < str.size())
            right++;

        reverse(str.begin() + left, str.begin() + right);
        left = right + 1;
        right++;
    }

    cout << str << endl;

    return 0;
}