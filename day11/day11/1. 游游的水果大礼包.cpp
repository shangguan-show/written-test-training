#define _CRT_SECURE_NO_WARNINGS

/* 解法：枚举*/
// 假设有 x 礼包 a, y 个礼包 b, 那么最大就是 ax + by
// 列出所有的 ax + by 的值，然后取其中的最大值

// 1.依次枚举 1 号礼包的个数 x。
// 2.根据 x 的值，计算出 2 号礼包的个数 y。
// 3.求出所有的 ax + by，然后求出最大值

/*求出 x y*/
// 1. 凑 1 号礼包：
// x 的取值范围：[0, min(n / 2, m)]
//	固定一个 x 之后，如何求出 y：选了 x 个苹果后，还剩 n - 2x 个苹果，桃子还剩 m - x 个， 
//	此时要在在 [n - 2x, m - x] 的范围内去凑 2 号礼包

// 2. 凑 2 号礼包：
// y 的取值范围：	min(n - 2x, (m - x) / 2)

#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	long long Max = 0;
	long long n = 0, m = 0, a = 0, b = 0;
	cin >> n >> m >> a >> b;

	// 枚举 1 号礼包的个数
	for (int x = 0; x <= min(n / 2, m); x++)
	{
		// 求出 2 号礼包的个数
		int y = min(n - 2 * x, (m - x) / 2);

		Max = max(a * x + b * y, Max);
	}

	cout << Max << endl;

	return 0;
}