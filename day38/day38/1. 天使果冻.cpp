#define _CRT_SECURE_NO_WARNINGS

// 解题思路：预处理 + 动态规划

// 1.状态表示
//	a.g[i] 表示前 i 个数中的 次大 值
//	b.f[i] 表示前 i 个数中的 最大 值

// 2.状态转移方程
//	a.当前值 arr[i] >= f[i]：f[i] = arr[i], g[i] = f[i]
//	b.当前值 g[i] <= arr[i] < f[i]，则 g[i] = arr[i]
//	c.
//		1.f[i] = max(f[i - 1], arr[i]);
//		2.g[i] = 
//			a.arr[i] >= f[i - 1] -> f[i - 1]
//			b.f[i - 1] > arr[i] >= g[i - 1] -> arr[i]

// 3.初始化
// g / f 表都初始化成 -∞

// 4.填表顺序
//	a.从前往后

// 5.返回值
//	a.g[x]

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int n, q, x;

int main()
{
	// 录入数据
	cin >> n;

	vector<int> arr(n + 1);
	for (size_t i = 1; i <= n; i++)
		cin >> arr[i];
	
	// 1.创建 dp 表
	vector<int> f(n + 1);	// 存最大值
	vector<int> g(n + 1);	// 存次大值

	// 2.初始化 dp 表
	f[1] = arr[1];
	g[1] = 0;

	// 3.填 dp 表 (更新最大值 & 次大值数组)
	for (size_t i = 2; i <= n; i++)
	{
		f[i] = max(f[i - 1], arr[i]);

		if (arr[i] >= f[i - 1])
			g[i] = f[i - 1];
		else if (arr[i] >= g[i - 1])
			g[i] = arr[i];
		else
			g[i] = g[i - 1];
	}

	cin >> q;

	while (q--)
	{
		cin >> x;
		cout << g[x] << endl;
	}

	return 0;
}