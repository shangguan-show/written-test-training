/* 操作解析 */
// 1.顺时针旋转 180° ： 分解成顺时针旋转两遍 90°
// 2.关于行镜像： 中间画一条虚线，上下两边的数据互换

/* 解题思路 */
// 1.180° 旋转	：
//	a.在原始矩阵上先来一次关于行对称互换数据的操作
//	b.在行操作的矩阵上来一次关于列对称互换数据的操作
// 2.关于行镜像	：
//	a.在原始矩阵上来一次关于行对称互换数据的操作

// 行对称操作：第 1 行和第 n 行交换，第 2 行和第 n - 1 行交换，其余同理。
// 列对称操作：第 1 列和第 n 列交换，第 2 列和第 n - 1 列交换，其余同理。

// 根据 x 的值，统计总共要进行多少次行操作和列操作
// 如果行对称操作的次数为偶数，则不用操作，会旋转回去，列操作同理

#include <vector>
#include <iostream>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;
using std::swap;
using std::vector;

int n = 0;
int q = 0;		// 询问次数
int x = 0;		// 每次询问对矩阵执行的操作
int row = 0;	// 统计行对称操作的次数
int col = 0;	// 统计列对称操作的次数

// 行对称操作
void set_row(vector<vector<int>>& arr)
{
	for (int i = 0; i < n / 2; i++)
		for (int j = 0; j < n; j++)
			swap(arr[i][j], arr[n - 1 - i][j]);
}

// 列对称操作
void set_col(vector<vector<int>>& arr)
{
	for (int j = 0; j < n / 2; j++)
		for (int i = 0; i < n; i++)
			swap(arr[i][j], arr[i][n - 1 - j]);
}

int main()
{
	cin >> n;

	vector<vector<int>> arr(n, vector<int>(n));
	for (size_t i = 0; i < n; i++)
		for (size_t j = 0; j < n; j++)
			cin >> arr[i][j];
	
	cin >> q;

	while (q--)
	{
		cin >> x;

		if (1 == x)	// 列操作数 + 1
			col++;
		row++;		// 两种操作均有一次行操作
	}

	row %= 2;
	col %= 2;

	if (row)
		set_row(arr);

	if (col)
		set_col(arr);

	for (size_t i = 0; i < n; i++)
	{
		for (size_t j = 0; j < n; j++)
			cout << arr[i][j] << " ";
		cout << endl;
	}
	return 0;
}