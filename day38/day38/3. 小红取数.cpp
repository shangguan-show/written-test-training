
// 01 背包 + 同余定理
//		01 背包：在一堆数中挑出一堆数满足总和为 k 的倍数的情况下的最大和

/* 1.状态表示 */
//	a.dp[i]表示：
//		1.从前 i 个数中挑选总和为 k 的倍数的最大和	
//		2.条件不足以推出状态转移方程, 无法判断第 i 个数能和前面的数拼出 k 的倍数
//	b.优化：同余定理：
//		1.a % k = x, b % k = y -> (a + b) % k == 0 <-> (x + y) % k == 0 
//	c.dp[i][j] 表示从前 i 个数中挑选总和 % k 等于 j 时的最大和

/* 2.状态转移方程 */
//	a.不选第 i 个值：
//		1.dp[i][j] = dp[i - 1][j]
//	b.挑选第 i 个值：
//		1. 从 j ~ i - 1 中挑选余数为 j - arr[i] % k 的值，防止减完之后变成负数要 + k，又为了防止 + k 之后超了，前面的结果要 % k
//		2.dp[i][j] = dp[i - 1][(j - arr[i] % k + k) % k] + arr[i]
//	c.求上述两种情况的最大值：dp[i][j] = max(dp[i - 1][j], dp[i - 1][(j - arr[i] % k + k) % k] + arr[i]);

/* 3.初始化 */
//	a.整张表都初始化成 -∞
//	b. dp[0][0] = 0

/* 4.填表顺序 */
//	a.从上往下每一行，每一行从左往右

/* 5.返回值 */
//	a.dp[n][0]，从前 n 个数挑选总和 % k 为 0 的最大和

#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

long long n, k, sum;
const int N = 1010;
long long arr[N];
long long dp[N][N];

int main()
{
	// 1.录入数据
	cin >> n >> k;
	for (int i = 1; i <= n; i++)
		cin >> arr[i];

	// 2.初始化 dp 表
	memset(dp, -0x3f, sizeof dp);
	dp[0][0] = 0;

	// 3.填 dp 表
	for (int i = 1; i <= n; i++)
		for (int j = 0; j < k; j++)
			dp[i][j] = max(dp[i - 1][j], dp[i - 1][(j - arr[i] % k + k) % k] + arr[i]);

	// 4.确定返回值
	if (dp[n][0] <= 0)
		cout << -1 << endl;
	else
		cout << dp[n][0] << endl;

	return 0;
}