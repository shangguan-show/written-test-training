#define _CRT_SECURE_NO_WARNINGS

#include <queue>
#include <vector>
#include <algorithm>

using namespace std;

 struct ListNode 
 {
    int val;
    struct ListNode *next;
 
    ListNode(int x) 
        : val(x), next(nullptr) 
    {}
 };

 // 解法一：利用小根堆
 //     a.将所有的结点扔进小根堆中，依次取出堆顶，然后拼在一起即可

// 解法二：优化解法一
//  只将每个链表的首结点放入堆中，再依次将首节点给拿出来
// 用完之后再将下一个结点入堆即可

 class Solution
 {
 public:
     struct cmp
     {
         bool operator()(ListNode* x, ListNode* y)
         {
             return x->val > y->val;
         }
     };
     ListNode* mergeKLists(vector<ListNode*>& lists)
     {
         if (1 == lists.size())
             return lists[0];

         priority_queue<ListNode*, vector<ListNode*>, cmp> pq;

         // 将所有链表的首结点入堆
         for (size_t i = 0; i < lists.size(); i++)
             if (lists[i] != nullptr)
                 pq.push(lists[i]);

         ListNode* ret = new ListNode(-1);
         ListNode* tail = ret;

         while (!pq.empty())
         {
             ListNode* tmp = pq.top();
             pq.pop();
             tail->next = tmp;
             tail = tail->next;

             // 还有下一个结点，则下一个结点入堆
             if (tmp->next != nullptr)
                 pq.push(tmp->next);
         }

         ListNode* res = ret->next;
         delete ret;

         return res;
     }
 };