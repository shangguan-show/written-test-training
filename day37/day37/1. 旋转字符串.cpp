#define _CRT_SECURE_NO_WARNINGS

#include <string>

using namespace std;

class Solution
{
public:
    bool solve(string A, string B)
    {
        for (size_t i = 0; i < A.size(); i++)
        {
            string tmp = A.substr(i);
            tmp += A.substr(0, i);

            if (tmp == B)
                return true;
        }

        return false;
    }
};