// 解题思路：暴力 -> 递归 -> 记忆化搜索

// 暴力：枚举每个格子都作为起点求其最长递减序列，然后求所有最长递减序列的最大值
// 递归：求出以某个点为起点的最长递减序列的长度
// 记忆化搜索：存储某个最长递减路径的下级最长递减路径

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

const int N = 110;

int n, m;
int arr[N][N];
int dx[4] = { 0,0,1,-1 };
int dy[4] = { 1,-1,0,0 };

static int dfs(int i, int j)
{
	int len = 1;	// 表示以当前位置为起点的最长递减路劲长度

	for (int k = 0; k < 4; k++)
	{
		int x = i + dx[k];
		int y = j + dy[k];
	
		if (x >= 1 && x <= n && y >= 1 && y <= m && arr[x][y] < arr[i][j])
			len = max(len, 1 + dfs(x, y));
	}

	return len;
}

int main()
{
	cin >> n >> m;

	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			cin >> arr[i][j];

	int ret = 1;

	for (int i = 1; i <= n; i++)		// 枚举所有的位置为起点，找最长递减序列
		for (int j = 1; j <= m; j++)
			ret = max(ret, dfs(i, j));	// 求所有以 i, j 为起点的最长的最长递减路线
	
	cout << ret << endl;

	return 0;
}