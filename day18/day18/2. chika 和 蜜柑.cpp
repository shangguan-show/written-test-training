#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

struct cmp
{
    bool operator()(const pair<int, int>& v1, const pair<int, int>& v2)
    {
        if (v1.second != v2.second) // 甜度不同时，按照甜度排，甜度高的在上面
            return v1.second > v2.second;
        else                        // 甜度相同时，按照酸度排，酸度低的在上面
            return v1.first < v2.first;
    }
};

const long long N = 2e5 + 10;

int main()
{
    long long n = 0, k = 0;
    cin >> n >> k;

    vector<pair<long long, long long>> arr(N);

    // 录入每个蜜柑的酸度
    vector<long long> sour(n);
    for (size_t i = 0; i < n; i++)
        cin >> arr[i].first;

    // 录入每个蜜柑的甜度
    vector<long long> sweet(n);
    for (size_t i = 0; i < n; i++)
        cin >> arr[i].second;

    sort(arr.begin(), arr.end(), cmp());

    long long sum_sour = 0;
    long long sum_sweet = 0;

    for (size_t i = 0; i < k; i++)
    {
        sum_sour += arr[i].first;
        sum_sweet += arr[i].second;
    }

    cout << sum_sour << " " << sum_sweet << endl;

    return 0;
}