#define _CRT_SECURE_NO_WARNINGS

#include <string>

using std::string;

class Solution
{
public:
    string compressString(string param)
    {
        string ret;
        int left = 0, right = 0;

        for (; left < param.size(); right++)
        {
            if (param[left] != param[right])
            {
                ret += param[left];

                if (right - left > 1)
                    ret += to_string(right - left);

                left = right;
            }
        }

        return ret;
    }
};