#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

// 解法：动态规划 - 背包问题

// 1.状态表示：dp[i][j]：表示从前 i 个物品中挑选，总体积不超过 j，此时的最大重量

// 2.状态转移方程：i 号物品有两种清空，放进背包和不放进背包
//	a.不选 i 号物品进背包：dp[i - 1][j] 最大重量在这里存着
//	b.选择 i 号物品进背包：dp[i - 1][j - v[i]] + w[i];
//		先去 0~i-1 处调总体加不超过 j - v[i] 的最大价值，然后 + 此时 i 位置的重量

// 3.细节问题：dp[0][j] = 0;

// 4.返回值：return dp[n][v];

// 5.空间优化：dp[j] = max(dp[j], dp[j - v[i]]);

class Solution
{
private:
	int dp[1010] = { 0 };

public:
	int knapsack(int v, int n, vector<vector<int>>& vw)
	{
		for (size_t i = 0; i < n; i++)
		{
			for (int j = v; j >= vw[i][0]; j--)
			{
				dp[j] = max(dp[j], dp[j - vw[i][0]] + vw[i][1]);
			}
		}

		return dp[v];
	}
};