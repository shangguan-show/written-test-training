#define _CRT_SECURE_NO_WARNINGS

// 题目要求最少删除多少人之后剩下的人能组成合唱序列
// 先求出最长的合唱序列 len，然后用 n - len 就能得出最少要删除多少人
// 用 i 位置将数组分成两部分，左边的部分是一个上升序列，右边的部分是一个下降序列
// 求左边的最长上升子序列 x，和右边的最长下降子序列 y
// x + y - 1 即为整个数组的合唱序列 (第 i 号同学计算了 2 次)

////////////////////////////////////////////////////////////////////////////////

// 解法：动态规划 - 最长上升子序列

// 1.状态表示
//	a.从前往后看：上升序列还是上升序列，f[i] 表示以 i 为结尾的最长 上升 子序列长度 x
//	b.从后往前看：下降序列变成上升序列，g[i] 表示以 i 为起点的最长 上升 子序列长度 y

// 2.返回值
//  a.从所有的 f[i] + g[i] - 1 中求一个最大值 max，然后用总长度 n - max 即可
//	b.n - max(f[i] + g[i] - 1); (1 <= i <= n)

////////////////////////////////////////////////////////////////////////////////

// 如何求最长上升子序列

// 1.状态表示
//	a.dp[i] 表示以 i 为结尾的所有子序列中，最长上升子序列的长度

// 2.状态转移方程
//	a.以 i 位置本身为最长上升子序列：dp[i] = 1;
//	b.从 [0, i - 1] 中如果有一个位置的值 j < arr[i] 那么 i 处的值就能加入到以 j 结尾的最长上升子序列中
//		1.arr[j] < arr[i] -> dp[j] + 1
//	c.求上述两种情况的最大值即可 dp [i] = max(dp[i], dp[j] + 1);

#include <vector>
#include <iostream>
#include <algorithm>

using std::max;
using std::cin;
using std::cout;
using std::endl;
using std::vector;

int main()
{
	int n = 0;
	cin >> n;

	vector<int> arr(n + 1);
	for (size_t i = 1; i <= n; i++)
		cin >> arr[i];

	vector<int> f(n + 1);			// 求从左往右的以 i 结尾的最长上升子序列长度
	vector<int> g(n + 1);			// 求从右往左的以 i 结尾的最长上升子序列长度

	// 求最长上升子序列
	for (int i = 1; i <= n; i++)
	{
		f[i] = 1;

		for (int j = 1; j < i; j++)
		{
			if (arr[j] < arr[i])	// i 处的值能够跟在 j 处的值后面
				f[i] = max(f[i], f[j] + 1);
		}
	}

	// 求最长下降子序列
	for (int i = n; i >= 1; i--)
	{
		g[i] = 1;

		for (int j = i + 1; j <= n; j++)
		{
			if (arr[j] < arr[i])
				g[i] = max(g[i], g[j] + 1);
		}
	}

	int len = 0;

	for (int i = 1; i <= n; i++)
		len = max(f[i] + g[i] - 1, len);

	cout << n - len << endl;

	return 0;
}