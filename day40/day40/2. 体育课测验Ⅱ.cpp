#define _CRT_SECURE_NO_WARNINGS

// 解题思路：拓扑排序

// 1.建图
// 2.将入度为 0 的点加入到队列中
// 3.来一次层序遍历，在遍历下一个结点时，要将其入度减 1

#include <queue>
#include <vector>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::queue;
using std::vector;

class Solution
{
public:
    vector<int> findOrder(int n, vector<vector<int>>& groups)
    {
        vector<vector<int>> edges(n);   // 存储边的信息
        vector<int> in(n);              // 存储每个结点的入度

        // 1.建图
        for (auto v : groups)
        {
            int a = v[0], b = v[1];     // 想完成 v[1] 就必须先完成 v[0]，构造由 b 指向 a 的一条边
            edges[b].push_back(a);      // b 后面连接着 a
            in[a]++;                    // a 的入度 + 1
        }

        // 2.将入度为 0 的点入队列
        queue<int> q;

        for (size_t i = 0; i < n; i++)
            if (0 == in[i])
                q.push(i);

        // 3.拓扑排序 (层序遍历)
        vector<int> ret;                // 统计拓扑排序后的结果

        while (!q.empty())
        {
            int a = q.front();
            q.pop();
            ret.push_back(a);

            for (auto b : edges[a])    // 遍历 a 所链接的点
            {
                if (--in[b] == 0)
                    q.push(b);
            }
        }

        if (n == ret.size())            // 有完成顺序
            return ret;
        
        return {};                      // 没有完成顺序，返回空数组
    }
};