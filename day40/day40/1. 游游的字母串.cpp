#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <string>
#include <climits>
#include <iostream>
#include <algorithm>

using namespace std;

string str;
int ret = INT_MAX;

int main()
{
    cin >> str;

    for (char ch = 'a'; ch <= 'z'; ch++)
    {
        int sum = 0;

        for (size_t i = 0; i < str.size(); i++)
            sum += min(abs(str[i] - ch), 26 - abs(str[i] - ch));

        ret = min(ret, sum);
    }

    cout << ret << endl;

    return 0;
}