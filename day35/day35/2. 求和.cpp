#define _CRT_SECURE_NO_WARNINGS

// 解题思路：dfs

#include <iostream>

using namespace std;

int n, m;
bool choose[11];	// 标记路径中选了哪些数
int sum;			// 标记选了的数的和

static void dfs(int x)
{
	if (sum == m)
	{
		for (int i = 1; i <= n; i++)
			if (choose[i])
				cout << i << " ";
		cout << endl;
		
		return;
	}

	if (sum > m || x > n)
		return;
	
	// 选择当前数
	sum += x;
	choose[x] = true;
	dfs(x + 1);
	sum -= x;
	choose[x] = false;

	// 不选当前数
	dfs(x + 1);
}

int main()
{	
	cin >> n >> m;

	dfs(1);	// 从 1 开始选或不选

	return 0;
}