using namespace std;
#define _CRT_SECURE_NO_WARNINGS

// 解题思路：动态规划

// 1.状态表示
//	a.选取 a 串的一段和 b 串的一段，先考虑 a 的这一段变成 b 的这一段的编辑距离
//	b.dp[i][j] 表示：字符串 a 中 [1, i] 区间的子串变成字符串 b 的 [1, j] 这段区间的子串的最少操作次数

// 2.状态转移方程
//	a.a[i] == b[j]：两个子区间最后的字符相同，只需要求 串 a [1, i - 1] 变成串 b [1, j -1] 的编辑距离即可	
//		1.dp[i][j] = dp[i - 1][j - 1]
//	b.a[i] != b[j]：两个子区间最后的字符不同
//		1.删除字符：让 a [1, i - 1] 变成 b [1, j]
//			a.dp[i - 1][j] + 1;
//		2.插入字符：在 i 后面插入一个 b[j]
//			a.dp[i][j - 1] + 1;
//		3.替换字符：将 a[i] 换成 b[j] 此时只需要让 [1, i - 1] 变成 [1, j - 1] 即可
//			a.dp[i - 1][j - 1] + 1
//		4.要的是最小编辑距离，求上述三种情况的 min 即可
//			a.dp[i][j] = min(dp[i - 1][j] + 1, min(dp[i][j - 1] + 1, dp[i - 1][j - 1] + 1));

// 3.初始化
//	a.将 dp 表第一行初始化成 0 1 2 3 ... 
//	b.将 dp 表第一列初始化成 0 1 2 3 ...

// 4.填表顺序
//	从上往下填每一行，从左往右填每一列

// 5.返回值
//	a.return dp[n][m]，返回串 a 变成串 b 的最少操作次数

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

string a, b;

int main()
{
    cin >> a;
    cin >> b;

    size_t n = a.size();
    size_t m = b.size();

    // 1.创建 dp 表
    vector<vector<int>> dp(1001, vector<int>(1001));

    // 2.初始化 dp 表
    for (size_t j = 0; j <= m; j++) // 初始化第一行
        dp[0][j] = j;
    for (size_t i = 0; i <= n; i++) // 初始化第一列
        dp[i][0] = i;

    // 3.填 dp 表
    for (size_t i = 1; i <= n; i++)
    {
        for (size_t j = 1; j <= m; j++)
        {
            if (a[i - 1] == b[j - 1])
                dp[i][j] = dp[i - 1][j - 1];
            else
                dp[i][j] = min(dp[i - 1][j] + 1, min(dp[i][j - 1] + 1, dp[i - 1][j - 1] + 1));
        }
    }

    // 4.确定返回值
    cout << dp[n][m] << endl;

    return 0;
}