#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <iostream>

using namespace std;

int main()
{
    long long n = 0;

    while (cin >> n)
    {
        int x = 0;
        for (x = 0; pow(2, x) - 1 <= n; x++);
        cout << (int)pow(2, x - 1) - 1 << endl;
    }

    return 0;
}