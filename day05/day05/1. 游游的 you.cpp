#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

using namespace std;

int main()
{
    int q = 0;
    cin >> q;

    int a = 0, b = 0, c = 0;

    while (q--)                  // 总共有 q 次查询
    {
        string tmp;
        cin >> a >> b >> c;

        int score_oo = 0;
        int score_you = 0;

        int min = (a < b ? (a < c ? a : c) : (b < c ? b : c));

        score_you += min * 2;   // 最小值是多少就有几个 you，分数 = you 个数 * 2
        b -= min;               // 找完 min 个 you 之后 o 的个数小消除 min 个 

        if (b > 0)              // oo 的分数为 o 的个数 - 1
            score_oo += b - 1;

        cout << score_you + score_oo << endl;
    }

    return 0;
}