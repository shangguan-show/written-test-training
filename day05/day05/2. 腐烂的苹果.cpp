#define _CRT_SECURE_NO_WARNINGS

#include <pair>
#include <queue>
#include <vector>
#include <iostream>

using std::pair;
using std::queue;
using std::vector;

// 解法：多源 BFS + 最短路
// 将所有腐烂的苹果统一加到一个队列中，每次出队的时候将当前队列的所有元素全部弄出去
// 当扩展到最后一个 1 时，扩展的层数就是分钟数

// 解题步骤
// 1.遍历矩阵，将所有腐烂的苹果 (值为 2) 入队列
// 2. 执行 BFS，BFS 时用一个变量 sz 标记队列中的元素个数，每次出队列时出 sz 个
// 3. 用一个 ret 变量来标记总共扩展了几层 (分钟数)
// 4. 最后再遍历一遍矩阵，如果有的 1 的位置没有访问过，则返回 -1，反之如果所有的 1 都被访问过，则返回最大步数 ret 即可

class Solution
{
private:
	int n = 0, m = 0;						// 记录矩阵的大小
	int dx[4] = { 0, 0, 1, -1 };			// 标记 x 的四个方向
	int dy[4] = { 1, -1, 0, 0 };			// 标记 y 的四个方向
	bool visit[1010][1010] = {0};			// 标记位置是否访问过
public:
	int rotApple(vector<vector<int>>& grid)
	{
		n = grid.size(), m = grid[0].size();

		queue<pair<int, int>> q;			// 记录腐烂苹果的下标

		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (2 == grid[i][j])		// 将源点 (最开始腐烂的苹果) 下标入队列
					q.push({ i, j });

		int ret = 0;						// 记录最大步数

		while (!q.empty())					// 队列不为空时病毒一直往外扩散
		{
			int sz = q.size();				// 记录当前层的元素个数
			ret++;							// 向外扩一层

			while (sz--)
			{
				auto [a, b] = q.front();	// 每次拿出队头元素
				q.pop();

				for (int i = 0; i < 4; i++)
				{
					int x = a + dx[i];		// 从 a 的四周开始找
					int y = b + dy[i];		// 从 b 的四周开始找
					
					// x y 处于合法范围，且 x y 处有苹果且是好苹果，并且准备访问的位置没被访问过
					if (x >= 0 && x < n && y >= 0 && y < m
						&& 1 == grid[x][y] && !visit[x][y])
					{
						visit[x][y] = true;// 让好苹果变烂		
						q.push({ x, y });  // 让烂苹果的位置入队列
					}
				}
			}
		}

		for (int i = 0; i < n; i++)			// 查找是否存在没有被遍历过的好苹果
			for (int j = 0; j < m; j++)
				if (1 == grid[i][j] && !visit[i][j])
					return -1;
		
		return ret - 1;							
	}
};