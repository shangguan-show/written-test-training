#define _CRT_SECURE_NO_WARNINGS

#include <list>
#include <cstddef>
#include <iostream>

using namespace std;

class Solution
{
public:
    int LastRemaining_Solution(int n, int m)
    {
        list<int> lt;
        int step = 1;

        for (int i = 0; i < n; i++)
            lt.push_back(i);

        list<int>::iterator it = lt.begin();

        while (lt.size() > 1)
        {
            if (it != lt.end())
            {
                if (0 == step % m)
                    it = lt.erase(it);
                else
                    it++;

                step++;
            }
            else 
            {
                it = lt.begin();    // 当 it 走到 end 时 (头结点)，重新指向首结点
            }
        }

        return lt.back();           // 返回链表中剩下的唯一一个元素
    }
};

int main()
{
    Solution s;
    cout << s.LastRemaining_Solution(5, 3) << endl;

    return 0;
}