#define _CRT_SECURE_NO_WARNINGS

// 解法: 图的遍历 - bfs / dfs

#include <iostream>

using namespace std;

class Solution
{
private:
    bool visit[210] = { 0 };    // 标记当前位置是否已经被搜索过
public:
    void dfs(vector<vector<int>>& m, int pos)
    {
        visit[pos] = true;      // 进入当前位置时，则访问量当前位置

        for (size_t i = 0; i < m.size(); i++)
            if (!visit[i] && m[pos][i])
                dfs(m, i);
    }

    int citys(vector<vector<int>>& m)
    {
        int ret = 0;
        int n = m.size();

        for (size_t i = 0; i < n; i++)
        {
            if (!visit[i])      // 没被访问过
            {
                ret++;
                dfs(m, i);
            }
        }

        return ret;
    }
};