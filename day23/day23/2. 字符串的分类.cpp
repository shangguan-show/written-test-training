#define _CRT_SECURE_NO_WARNINGS

// 将所有的字符串排序，再将所有的字符串放进一个 set 容器的 hash 表中
// hash 表中元素的个数即为字符串的种类

#include <set>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int n = 0;
    cin >> n;

    set<string> hash;

    for (size_t i = 0; i < n; i++)
    {
        string str;
        cin >> str;
        sort(str.begin(), str.end());
        hash.insert(str);
    }

    cout << hash.size() << endl;

    return 0;
}