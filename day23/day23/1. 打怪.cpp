#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

using namespace std;

int main()
{
    int t = 0;
    cin >> t;

    while (t--)
    {
        int warrior_HP = 0, warrior_ATK = 0;    // 勇士的血量和攻击力
        int monster_HP = 0, monster_ATK = 0;    // 怪物的血量和攻击力

        cin >> warrior_HP >> warrior_ATK >> monster_HP >> monster_ATK;

        int kill_count = 0;                     // 统计击杀怪物的数量
        int tmp = monster_HP;                   // 暂时保存怪物的血量

        // 野怪没攻击力 或 勇士能一刀斩杀野怪时可以无限击杀
        if (monster_ATK <= 0 || warrior_ATK >= monster_HP)
        {
            cout << -1 << endl;
        }
        else
        {
            while (warrior_HP > 0)
            {
                monster_HP -= warrior_ATK;      // 勇士先手攻击

                if (monster_HP <= 0)            // 怪物死亡时击杀数量 + 1
                {
                    monster_HP = tmp;           // 出现新的怪物
                    kill_count++;
                }
                else                            // 怪物死亡时，本回合不会受到攻击
                {
                    warrior_HP -= monster_ATK;  // 怪物后手攻击
                }
            }

            cout << kill_count << endl;
        }
    }

    return 0;
}