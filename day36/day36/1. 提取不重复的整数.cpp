#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <string>
#include <iostream>

using namespace std;

long long n;
map<int, int> m;
string ret;

int main()
{
    cin >> n;

    for (; n; n /= 10)
    {
        int tmp = n % 10;
        m[tmp]++;

        if (1 == m[tmp])
            ret += tmp + '0';
    }

    cout << ret << endl;

    return 0;
}