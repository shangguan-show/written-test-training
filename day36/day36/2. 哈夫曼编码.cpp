#define _CRT_SECURE_NO_WARNINGS

#include <queue>
#include <vector>
#include <iostream>

using namespace std;

long long n, tmp, ret;

int main()
{
    cin >> n;

    priority_queue<long long, vector<long long>, greater<long long>> pq;
    for (size_t i = 0; i < n; i++)
    {
        cin >> tmp;
        pq.push(tmp);
    }

    // 构建哈夫曼树
    // 每次拿出最小的两个数，累加完之后重新放入小根堆中，直到堆只剩下一个元素即可
    while (1 < pq.size())
    {
        long long tmp1 = pq.top();
        pq.pop();
        long long tmp2 = pq.top();
        pq.pop();
        pq.push(tmp1 + tmp2);
        ret += tmp1 + tmp2;
    }

    cout << ret << endl;

    return 0;
}

