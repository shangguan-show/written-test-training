#define _CRT_SECURE_NO_WARNINGS

// 解题思路：动态规划 + 哈希

#include <vector>
#include <string>
#include <iostream>

using namespace std;

string str;
long long n, ret;
long long f[26], g[26];

int main()
{
	cin >> n;
	cin >> str;

	for (size_t i = 0; i < n; i++)
	{
		int x = str[i] - 'a';
		ret += f[x];
		f[x] = f[x] + i - g[x];
		g[x] = g[x] + 1;
	}
	 
	cout << ret << endl;

	return 0;
}