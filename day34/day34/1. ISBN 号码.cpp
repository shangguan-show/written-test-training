#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <string>
#include <iostream>

using namespace std;

int main()
{
    string isbn;
    cin >> isbn;

    int k = 1;
    long long sum = 0;
    int n = isbn.size();

    for (size_t i = 0; i < n - 1; i++)
    {
        if (isbn[i] != '-')
        {
            sum += (isbn[i] - '0') * k;
            k++;
        }
    }

    sum %= 11;
    string idcode = "0123456789X";  // ʶ����

    if (idcode[sum] == isbn.back())
    {
        cout << "Right" << endl;
    }
    else
    {
        isbn[n - 1] = idcode[sum];
        cout << isbn << endl;
    }

    return 0;
}