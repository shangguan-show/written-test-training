#define _CRT_SECURE_NO_WARNINGS

#include <queue>
#include <vector>
#include <climits>
#include <iostream>
#include <algorithm>

using namespace std;

// 解题思路：bfs
//	弄一个距离数组 distance 记录到某个点的距离，然后用 bfs 将这个数组填满即可

int n, m;
int x1, y1;	// 标记起点位置
int dx[4] = { 0,0,1,-1 };
int dy[4] = { 1,-1,0,0 };
queue<pair<int, int>> q;

void bfs(vector<vector<int>>& distance, vector<vector<char>>& char_arr)
{
	distance[x1][y1] = 0;	// 起点置为 0
	q.push({ x1,y1 });		// 将起点入队列

	while (!q.empty())
	{	
		auto [x2, y2] = q.front();
		q.pop();

		for (size_t i = 0; i < 4; i++)
		{
			int a = x2 + dx[i];
			int b = y2 + dy[i];
			
			// 新坐标合法、新坐标没去过、新坐标能去
			if (a >= 1 && a <= n && b >= 1 && b <= m &&
				-1 == distance[a][b] && char_arr[a][b] != '*')
			{
				distance[a][b] = distance[x2][y2] + 1;

				if (char_arr[a][b] != 'e')
					q.push({ a,b });
			}
		}
	}
}

int main()
{
	cin >> n >> m;

	vector<vector<char>> char_arr(n + 1, vector<char>(m + 1));
	vector<vector<int>> distance(n + 1, vector<int>(m + 1, -1));

	for (size_t i = 1; i <= n; i++)
	{
		for (size_t j = 1; j <= m; j++)
		{
			cin >> char_arr[i][j];

			if ('k' == char_arr[i][j])	// 读到了一个起点位置
			{
				x1 = i;
				y1 = j;
			}
		}
	}

	bfs(distance, char_arr);

	int count = 0;						// 统计有多少个出口能够到达
	int ret = INT_MAX;					// 起点到出口的最短距离

	for (size_t i = 1; i <= n; i++)
	{
		for (size_t j = 1; j <= m; j++)
		{
			// 当前位置是出口且能到达
			if ('e' == char_arr[i][j] && distance[i][j] != -1)
			{
				count++;
				ret = min(ret, distance[i][j]);
			}
		}
	}

	if (0 == count)						// 没有出口
		cout << -1 << endl;
	else								// 输出出口数量和最短距离
		cout << count << " " << ret << endl;

	return 0;
}