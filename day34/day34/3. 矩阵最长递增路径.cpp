#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

// 解题思路：记忆化搜索 / 带备忘录的动态规划 / 带备忘录的递归

class Solution
{
private:
	int ret = 1;
	int n = 0, m = 0;
	int dx[4] = { 0,0,1,-1 };
	int dy[4] = { 1,-1,0,0 };
	int memo[1010][1010];
public:
	int dfs(vector<vector<int>>& matrix, int x, int y)
	{
		// 该位置的值已经计算过
		if (-1 != memo[x][y])
			return memo[x][y];

		int len = 1;

		for (size_t i = 0; i < 4; i++)
		{
			int a = x + dx[i];
			int b = y + dy[i];

			if (a >= 0 && a < n && y >= 0 && y < m && matrix[a][b] > matrix[x][y])
				len = max(len, dfs(matrix, a, b) + 1);
		}

		memo[x][y] = len;
		return len;
	}

	int solve(vector<vector<int>>& matrix)
	{
		n = matrix.size();
		m = matrix[0].size();
		
		memset(memo, -1, sizeof memo);

		// 求出所有以 [i, j] 为起点的最长递增路径
		for (size_t i = 0; i < n; i++)
			for (size_t j = 0; j < m; j++)
				ret = max(ret, dfs(matrix, i, j));

		return ret;
	}
};