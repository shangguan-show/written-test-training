#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

// 简单多状态 dp 问题

int main()
{
    int n = 0;
    cin >> n;
    vector<long long> arr(n);

    for (size_t i = 0; i < n; i++)
        cin >> arr[i];

    // 1.创建 dp 表
    vector<long long> f(n); // 记录选择 i 位置时的最大和
    vector<long long> g(n); // 记录不选 i 位置时的最大和

    // 2.初始化 dp 表
    f[0] = arr[0];          // 选择第一个数时的最大和就是原数组的第一个数
    g[0] = 0;               // 不选第一个数时的最大和自然是 0

    // 3.填 dp 表
    for (size_t i = 1; i < n; i++)
    {   
        // 选择 i 位置时的最大和等于不选前一个位置的最大和 + 当前位置的值
        f[i] = g[i - 1] + arr[i];
        // 不选 i 位置时的最大和等于前一个位置的最大和，前一个位置的最大和从选和不选中获取
        g[i] = max(g[i - 1], f[i - 1]);
    }

    // 4.确定返回值
    cout << max(g[n - 1], f[n - 1]);

    return 0;
}