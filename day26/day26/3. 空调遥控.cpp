#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int n = 0, p = 0;
    cin >> n >> p;

    vector<int> arr(n);
    
    for (size_t i = 0; i < n; i++)
        cin >> arr[i];
    
    int max_length = 0;
    int left = 0, right = 1;
    
    sort(arr.begin(), arr.end());

    for (; right < arr.size(); right++)
    {
        while ((arr[right] - arr[left] > 2 * p) && (left < right))
            left++; 
        
        max_length = max(max_length, right - left + 1);
    }

    cout << MAX << endl;

    return 0;
}