#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>

using namespace std;

bool is_back_str(const string& tmp)
{
    int left = 0, right = tmp.size() - 1;

    while (left < right)
        if (tmp[left++] != tmp[right--])
            return false;

    return true;
}

int main()
{
    string str;
    cin >> str;

    int min_len = 0;

    string two;
    string three;

    for (size_t i = 0; i < str.size(); i++)
    {
        two += str[i];
        three += str[i];

        while (two.size() > 2)
            two.erase(0, 1);
        while (three.size() > 3)
            three.erase(0, 1);

        if (2 == two.size() && is_back_str(two))
        {
            min_len = 2;
            break;
        }
        else if (3 == three.size() && is_back_str(three))
        {
            min_len = 3;
        }
    }

    if (min_len > 1)
        cout << min_len << endl;
    else
        cout << -1 << endl;

    return 0;
}