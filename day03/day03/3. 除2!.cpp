#define _CRT_SECURE_NO_WARNINGS

#include <queue>
#include <vector>
#include <iostream>

using namespace std;

#include <queue>
#include <vector>
#include <iostream>

using namespace std;

int main()
{
    long long n = 0, k = 0, sum = 0;
    cin >> n >> k;

    priority_queue<long long> pq;

    while (n--)
    {
        long long val = 0;
        cin >> val;
        sum += val;                     // 将输入的所有数 + 到 sum 中

        if (0 == val % 2)
            pq.push(val);               // 用大根堆存放偶数
    }

    while (!pq.empty() && k--)          // 堆不为空且还能操作时
    {
        long long tmp = pq.top() / 2;   // 取出堆顶最大的偶数 / 2
        pq.pop();                       // 将堆顶 (最大的偶数去掉)
        sum -= tmp;                     // 将所有元素的和剪掉堆顶的一半

        if (0 == tmp % 2)               // 将堆顶元素 / 2 后还是偶数的值放回大堆
            pq.push(tmp);
    }

    cout << sum << endl;

    return 0;
}