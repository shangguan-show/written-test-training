#define _CRT_SECURE_NO_WARNINGS

#include <cctype>
#include <string>
#include <iostream>

using namespace std;

int main()
{
    string str;
    string tmp;
    getline(cin, str);

    int left = 0, right = 0;

    while (left < str.size())
    {
        if (' ' == str[right] || right == str.size())
        {
            tmp += toupper(str[left]);
            left = right + 1;
        }

        right++;
    }

    cout << tmp << endl;

    return 0;
}