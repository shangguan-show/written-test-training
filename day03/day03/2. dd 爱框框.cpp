#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <climits>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int n = 0, x = 0;
    cin >> n >> x;

    vector<int> nums(n);

    for (int i = 0; i < n; i++)
        cin >> nums[i];

    int len = INT_MAX, sum = 0;
    int left = 0, right = 0;
    vector<int> ret(2);

    for (; right < nums.size(); right++)
    {
        sum += nums[right];             // 让 right 指向的值进窗口

        while (sum >= x)                // sum + 到符合条件时
        {
            if (len > right - left + 1) // 更新最短区间
            {
                ret[0] = left + 1;
                ret[1] = right + 1;
                len = right - left + 1;
            }

            sum -= nums[left++];        // 退窗口然后 left 右移
        }
    }

    cout << ret[0] << " " << ret[1] << endl;

    return 0;
}