#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    string solve(string s, string t)
    {
        int len1 = s.size();
        int len2 = t.size();
        vector<int> tmp(len1 + len2 - 1);

        reverse(s.begin(), s.end());
        reverse(t.begin(), t.end());

        for (size_t i = 0; i < len1; i++)
            for (size_t j = 0; j < len2; j++)
                tmp[i + j] += (s[i] - '0') * (t[j] - '0');

        string ret;
        int cur = 0;
        int next = 0;

        while (cur < len1 + len2 - 1 || next != 0)
        {
            if (cur < len1 + len2 - 1)
                next += tmp[cur++];

            ret += next % 10 + '0';
            next /= 10;
        }

        while (ret.size() > 1 && '0' == ret.back())
            ret.pop_back();

        reverse(ret.begin(), ret.end());

        return ret;
    }
};