#define _CRT_SECURE_NO_WARNINGS

#include <string>

using namespace std;

 struct ListNode 
 {
     int val;
     struct ListNode *next;

     ListNode(int x)    // 初始化列表
         : val(x) 
         , next(nullptr) 
     {}
  };

#include <vector>

 class Solution
 {
 public:
     string add_string(string s, string t)
     {
         int next = 0;
         int end1 = s.size() - 1;
         int end2 = t.size() - 1;
         string retstr;

         while (end1 >= 0 || end2 >= 0)
         {
             int val1 = end1 >= 0 ? s[end1--] - '0' : 0;
             int val2 = end2 >= 0 ? t[end2--] - '0' : 0;
             int ret = val1 + val2 + next;

             next = ret / 10;
             ret = ret % 10;
             retstr += ret + '0';
         }

         if (1 == next)
             retstr += '1';

         reverse(retstr.begin(), retstr.end());

         return retstr;
     }

     ListNode* addInList(ListNode* head1, ListNode* head2)
     {
         string str1, str2;
         ListNode* newlist = new ListNode(0);

         // 将链表 1 的内容尾插到 str1 中
         while (head1)    
         {
             str1 += head1->val + '0';
             head1 = head1->next;
         }

         // 将链表 2 的内容尾插到 str2 中
         while (head2)
         {
             str2 += head2->val + '0';
             head2 = head2->next;
         }

         // 对这两个字符串执行字符串相加
         string tmp = add_string(str1, str2);
         ListNode* head = newlist;

         // 将字符串相加后返回的字符串从头到尾取出每个字符尾插到链表中
         for (size_t i = 0; i < tmp.size(); i++)
         {
             ListNode* val = new ListNode(tmp[i] - '0');
             newlist->next = val;
             newlist = newlist->next;
         }

         return head->next;
     }
 };