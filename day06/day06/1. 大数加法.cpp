#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>

using namespace std;

class Solution 
{
public:
    string solve(string s, string t)
    {
        int next = 0;
        int end1 = s.size() - 1;
        int end2 = t.size() - 1;
        string retstr;

        while (end1 >= 0 || end2 >= 0)
        {
            int val1 = end1 >= 0 ? s[end1--] - '0' : 0;
            int val2 = end2 >= 0 ? t[end2--] - '0' : 0;
            int ret = val1 + val2 + next;

            next = ret / 10;
            ret = ret % 10;
            retstr += ret + '0';
        }

        if (1 == next)
            retstr += '1';

        reverse(retstr.begin(), retstr.end());

        return retstr;
    }
};