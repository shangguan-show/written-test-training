#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

using namespace std;

long long a, h, b, k;

int main()
{
   cin >> a >> h >> b >> k;

   long long count_harm = 0;    // 统计小红受到的总伤害

   while (h > 0 && k > 0)
   {
       count_harm += a;
       count_harm += b;
       h -= b;
       k -= a;
   }

   if (h <= 0 && k > 0)        // 对立死了，光活着，小红受到光的十倍伤害
       count_harm += b * 10;
   else if (k <= 0 && h > 0)   // 光活着，对立死了，小红受到对立的十倍伤害
       count_harm += a * 10;

   cout << count_harm << endl;

   return 0;
}