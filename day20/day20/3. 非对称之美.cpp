#define _CRT_SECURE_NO_WARNINGS

// 解题思路：找规律 / 贪心

// 先看整个字符串是不是回文串，如果是回文串则缩小字符串；
// 直到遇到的第一个非回文串，则是最长非回文子串

#include <string>
#include <iostream>

using namespace std;

string str;

// 返回最长非回文子串的长度
static int is_unback()
{
	// 1.判断是否全都是相同字符
	int flag = 0;

	for (int i = 1; i < str.size(); i++)
	{
		if (str[i] != str[0])
		{
			flag = 1;
			break;
		}
	}

	
	if (0 == flag)	// 全是同样的字符，非回文子串长度为 0
		return 0;

	// 2.判读本身是否是回文串
	int left = 0, right = str.size() - 1;
	flag = 1;
	while (left < right)
	{
		if (str[left] == str[right])
		{
			left++;
			right--;
		}
		else
		{
			flag = 0;
			break;
		}
	}

	if (flag)
		return str.size() - 1;

	return str.size();
}

int main()
{
	cin >> str;

	cout << is_unback() << endl;

	return 0;
}