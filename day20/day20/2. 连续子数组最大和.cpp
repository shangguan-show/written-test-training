#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    long long n = 0;
    cin >> n;

    vector<long long> arr(n + 1);
    for (long long i = 1; i <= n; i++)
        cin >> arr[i];

    vector<long long> dp(n + 1);
    for (long long i = 1; i <= n; i++)
        dp[i] = max(dp[i - 1], (long long)0) + arr[i];

    long long Max = dp[1];

    for (size_t i = 1; i < dp.size(); i++)
        if (Max < dp[i])
            Max = dp[i];

    cout << Max << endl;

    return 0;
}