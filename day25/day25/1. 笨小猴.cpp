#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <cmath>
#include <vector>
#include <climits>
#include <iostream>
#include <algorithm>

using namespace std;

static bool is_prime(int n)
{
    if (0 == n || 1 == n)
        return false;

    for (size_t i = 2; i <= sqrt(n); i++)
        if (0 == n % i)
            return false;

    return true;
}

int main()
{
    string str;
    map<int, int> hash;
    cin >> str;

    for (auto ch : str)
        hash[ch]++;

    int MAX = INT_MIN;
    int MIN = INT_MAX;

    for (auto ch : str)
    {
        MAX = max(MAX, hash[ch]);
        MIN = min(MIN, hash[ch]);
    }

    if (is_prime(MAX - MIN))    // 差值是质数
    {
        cout << "Lucky Word" << endl;
        cout << MAX - MIN << endl;
    }
    else                        // 差值非质数
    {
        cout << "No Answer" << endl;
        cout << 0 << endl;
    }

    return 0;
}