#define _CRT_SECURE_NO_WARNINGS

// 解题思路：动态规划 - 01背包

// 1.状态表示
//	a.dp[i][j] 表示，从前 i 个数中挑选能否凑成总和恰好为 j 的

// 2.状态转移方程
//	a.选择第 i 个数：i 处的值会影响结果，从 0 ~ i - 1 个数中挑选能够凑成总和 j - arr[i] 的
//		1.dp[i - 1][j - arr[i]]
//		2.j >= arr[i]
//	b.不选第 i 个数：i 处的值不影响结果，从 0 ~ i - 1 个数中挑选能够凑成总和 j 的
//		1.dp[i - 1][j]
//	c.dp[i][j] = dp[i - 1][j] || dp[i - 1][j - arr[i]];

// 3.初始化
//	a.dp[0][0] = true;

// 4.填表顺序
//	a.自上而下，自左而右

// 5.返回值
//	a.dp[n][sum / 2] 从前 n 个数是否能挑出总和为数组的一半的数

#include <vector>
#include <iostream>

using namespace std;

int main()
{
    int n = 0, sum = 0;
    cin >> n;
    vector<int> arr(n);

    for (size_t i = 0; i < n; i++)
    {
        cin >> arr[i];
        sum += arr[i];
    }

    if (1 == sum % 2)    // 总和如果是奇数永远也挑不出一半
    { 
        cout << "false" << endl;
    }
    else
    {
        // 1.创建 dp 表
        vector<vector<bool>> dp(n + 1, vector<bool>(500 * 100 + 10, false));

        // 2.初始化
        dp[0][0] = true;

        // 3.填 dp 表
        for (size_t i = 1; i <= n; i++)
        {
            for (size_t j = 0; j <= sum / 2; j++)
            {
                dp[i][j] = dp[i - 1][j];

                if (j >= arr[i - 1])
                    dp[i][j] = dp[i - 1][j] || dp[i - 1][j - arr[i - 1]];
            }
        }

        // 确定返回值
        if (dp[n][sum / 2])
            cout << "true" << endl;
        else
            cout << "false" << endl;
    }

    return 0;
}