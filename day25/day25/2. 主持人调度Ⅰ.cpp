#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <algorithm>

using namespace std;

#include <vector>
class Solution
{
public:
    bool hostschedule(vector<vector<int>>& schedule)
    {
        vector<int> start, end;

        for (size_t i = 0; i < schedule.size(); i++)
        {
            start.push_back(schedule[i][0]);
            end.push_back(schedule[i][1]);
        }

        sort(start.begin(), start.end());
        sort(end.begin(), end.end());

        for (size_t i = 1; i < schedule.size(); i++)
            if (start[i] < end[i - 1])
                return false;

        return true;
    }
};