#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>

using namespace std;

int main()
{
    string str;
    string tmp;                     // 用 tmp 串来充当栈
    cin >> str;

    tmp += str[0];                  // 字符串首元素先放入栈顶

    for (size_t i = 1; i < str.size(); i++)
    {
        if (str[i] == tmp.back())   // 指向的字符和栈顶字符相等时，删除栈顶字符
            tmp.pop_back();
        else                        // 不相等时将该字符入栈
            tmp += str[i];
    }

    if (tmp.empty())                // 栈为空时输出 0
        cout << 0 << endl;
    else                            // 不为空时输出消除后的字符串
        cout << tmp << endl;

    return 0;
}