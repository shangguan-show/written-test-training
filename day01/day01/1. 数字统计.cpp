#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

using namespace std;

long long get_count(long long target, long long val)
{
    long long count = 0;

    while (val)
    {
        if (2 == val % 10)
            count++;
        val /= 10;
    }

    return count;
}

int main()
{
    long long l = 0;
    long long r = 0;
    long long sum = 0;
    cin >> l >> r;

    for (size_t i = l; i <= r; i++)
        sum += get_count(l, i);

    cout << sum << endl;

    return 0;
}