#define _CRT_SECURE_NO_WARNINGS

#include <vector>

using namespace std;

class Solution
{
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2)
    {
        vector<int> hash(1001);

        for (int i = 0; i < nums1.size(); i++)
            for (int j = 0; j < nums2.size(); j++)
                if (nums1[i] == nums2[j])   // 找到公共元素就让 hash 表以该元素为下标处的值 + 1
                    hash[nums1[i]]++;

        vector<int> ret;

        for (int i = 1; i < hash.size(); i++)
            if (0 != hash[i])               // hash 表中不为 0 的位置下标都是公共元素
                ret.push_back(i);

        return ret;
    }
};