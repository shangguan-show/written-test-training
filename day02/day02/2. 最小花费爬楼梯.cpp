#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()  // ��̬�滮
{
    size_t n = 0;
    cin >> n;

    vector<int> cost(n);

    for (size_t i = 0; i < n; i++)
        cin >> cost[i];

    vector<size_t> sum(n);

    for (size_t i = 2; i < n; i++)
        sum[i] = min(sum[i - 1] + cost[i - 1], sum[i - 2] + cost[i - 2]);

    cout << min(sum[n - 1] + cost[n - 1], sum[n - 2] + cost[n - 2]) << endl;

    return 0;
}