#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int n = 0;
    cin >> n;
    string str1, str2;
    cin >> str1 >> str2;
    vector<string> strs(n);

    for (int i = 0; i < n; i++)
        cin >> strs[i];

    int distance = strs.size();
    int index1 = -1, index2 = -1;

    for (int i = 0; i < strs.size(); i++)
    {
        if (str1 == strs[i])                // 找到了串 1
            index1 = i;
        if (str2 == strs[i])                // 找到了串 2
            index2 = i;
        if (index1 != -1 && index2 != -1)   // 找到了两个串，就和先前找到的一对串的距离比较
            distance = min(distance, abs(index1 - index2));
    }

    if (index1 != -1 && index2 != -1)       // 两个串都在 strs 数组中找到时，输出最短距离
        cout << distance << endl;
    else                                    // 只要有一个没找到就输出 -1
        cout << -1 << endl;

    return 0;
}