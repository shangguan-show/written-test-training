#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
using namespace std;

int main()
{
    float a = 0.0;      // 表示要寄的快递重量
    char b = 0;         // 表示是否加急 'y':加急 'n' 不加急
    int price = 0;
    cin >> a >> b;

    if ('y' == b)       // 加急算法
    {
        price = 25;     // 基础 20 元 + 加急 5 元

        if (a > 1.0)
        {
            if (a - (int)a > 0.0)
                price += (int)a;
            else
                price += (int)a - 1;
        }
    }
    else if ('n' == b)  // 不加急算法
    {
        price = 20;

        if (a > 1.0)
        {
            if (a - (int)a > 0.0)
                price += (int)a;
            else
                price += (int)a - 1;
        }
    }

    cout << price << endl;

    return 0;
}