// 哈夫曼编码

#include <map>
#include <queue>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

string str;

int main()
{
	while (cin >> str)
	{
		map<char, int> m;
		priority_queue<int, vector<int>, greater<int>> pq;

		// 1.统计每个字符的频次
		for (auto ch : str)
			m[ch]++;

		// 2.将所有的频次放进堆中
		for (auto x : m)
			pq.push(x.second);

		// 3.将最小的两个元素相加后放入堆中
		int ret = 0;
		while (pq.size() > 1)
		{
			int x = pq.top();
			pq.pop();
			int y = pq.top();
			pq.pop();
			ret += x + y;
			pq.push(x + y);
		}

		cout << ret << endl;
	}

	return 0;
}