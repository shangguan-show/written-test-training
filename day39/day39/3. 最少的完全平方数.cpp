// 解题思路：dp - 完全背包

/*1.状态表示*/
//	a.dp[i][j] 表示：从前 i 个数中挑选总和刚和为 j 时，最少挑出来几个数

/*2.状态转移方程*/
//	a.选择第 i 个数：
//		1.选 1 个 i：从 1 ~ i - 1 中凑 j - i * i, dp[i - 1][j - i * i] + 1;
//		2.选 2 个 i：从 1 ~ i - 1 中凑 j - 2 * i * i, dp[i - 1][j - 2 * i * i] + 2;
//		3.之后的以此类推...
//		4.这所有的状态都可以修改成 dp[i][j - i * i] + 1;
//	b.不选第 i 个数：
//		1.从 1 ~ i - 1 个数挑，dp[i][j] = dp[i - 1][j]
//	c.求上述两种情况的最小值：
//		1.dp[i][j] = min(dp[i - 1][j], dp[i][j - i * i] + 1);
//		2.dp[i][j - i * i] + 1 要想存在必须保证 j >= i * i

/*3.初始化*/
//	a.dp[0][0] = 0
//	b.第一行的其余数为 +∞

/*4.填表顺序*/
//	a.自上而下，自左而右

/*5.返回值*/
//	a.dp[根号n][n]

#include <cmath>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	int n = 0;
	cin >> n;
	vector<int> dp(n + 1, 0x3f3f3f3f);
	dp[0] = 0;

	for (int i = 1; i <= sqrt(n); i++)
		for (int j = i * i; j <= n; j++)
			dp[j] = min(dp[j], dp[j - i * i] + 1);

	cout << dp[n] << endl;

	return 0;
}