#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>

using namespace std;

int main()
{
	string str;
	int hash[26] = { 0 };

	while (getline(cin, str))
		for (auto ch : str)
			if (isalpha(ch))
				hash[ch - 'a']++;

	int maxi = 0;

	for (size_t i = 1; i < 26; i++)
		if (hash[i] > hash[maxi])
			maxi = i;

	cout << static_cast<char>(maxi + 'a') << endl;

	return 0;
}