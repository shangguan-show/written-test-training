#define _CRT_SECURE_NO_WARNINGS

// 解题思路：动态规划 - 区间 dp

// 1.状态表示
//  dp[i][j] 表示字符串 [i,j] 区间内，最长回文子序列的长度

// 2.状态转移方程
//  a.如果 i > j：不存在这个区间，dp[i][j] = 0
//  b.如果 i = j：指向同一个字符，自己就能构成回文串，dp[i][j] = 1
//  c.如果 i < j：
//   1.i 和 j 所指向的字符相同 s[i] = s[j]：
//      从 [i+1, j-1] 中挑一个最长回文子序列，然后补充一下 i j 位置的字符
//      dp[i][j] = dp[i + 1][j - 1] + 2;
//   2.i 和 j 所指向的字符不同 s[i] ≠ s[j]：从 [i,j-1] 和 [i+1, j] 中挑一个最长回文子序列，不选左右两个端点 i j 位置的字符
//      dp[i][j] = max(dp[i][j - 1], dp[i + 1][j]);

// 3.初始化
//  a.在填 [i,j] 时有可能会用到 i + 1 和 j - 1 位置的值

// 4.填表顺序
//  从下往上填每一行，每一行从左往右填

// 5.返回值
//  dp[0][n - 1]，0 ~ n-1 的最长回文子序列的长度

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int longestPalindromeSubSeq(string s)
    {
        int n = s.size();
        vector<vector<int>> dp(n, vector<int>(n));
        
        for (int i = n - 1; i >= 0; i--)
        {
            // 处理 i = j 的情况
            dp[i][i] = 1;   

            // 处理 i < j 的情况
            for (int j = i + 1; j < n; j++)
            {
                if (s[i] == s[j])
                    dp[i][j] = dp[i + 1][j - 1] + 2;
                else
                    dp[i][j] = max(dp[i][j - 1], dp[i + 1][j]);
            }
        }

        return dp[0][n - 1];
    }
};