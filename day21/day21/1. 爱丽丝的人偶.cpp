#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>

using namespace std;

// 将这 n 个数按照从大到小，从左往右的顺序以间隔为 2 往数组内填

//           1 2 3 4 5 6 7 8 9
// 第一趟：   9   8   7   6   5
// 第二趟：     4    3   2  1 
// 依次类推就能让除了首尾两个元素之外的所有元素的两边都是比其小/大了

int main()
{
    int n = 0;
    cin >> n;

    vector<int> arr(n);

    for (size_t i = 0; n; i += 2)
    {
        if (i >= arr.size())
            i = 1;

        arr[i] = n--;
    }

    for (auto val : arr)
        cout << val << " ";
    cout << endl;

    return 0;
}