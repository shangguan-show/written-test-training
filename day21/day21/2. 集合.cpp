#define _CRT_SECURE_NO_WARNINGS

#include <set>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

// 将两个集合中的所有数都放到一个 set 对象中去重
// 然后将 set 对象中的所有元素都放到一个数组中，最后对数组排升序即可

int main()
{
    int n = 0, m = 0;
    cin >> n >> m;
    set<int> s;

    for (size_t i = 0; i < n; i++)
    {
        int val = 0;
        cin >> val;
        s.insert(val);
    }

    for (size_t i = 0; i < m; i++)
    {
        int val = 0;
        cin >> val;
        s.insert(val);
    }

    vector<int> ret;
    for (auto val : s)
        ret.push_back(val);

    sort(ret.begin(), ret.end());

    for (auto val : ret)
        cout << val << " ";
    cout << endl;

    return 0;
}