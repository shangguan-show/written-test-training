﻿#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

int n;
string str;

int main()
{
    cin >> n;
    cin >> str;

    // 求整个串的 0 1 的数量
    int zero = 0, one = 0;
    for (auto ch : str)
    {
        if ('1' == ch)
            one++;
        if ('0' == ch)
            zero++;
    }

    int ret = 0;
    int left = 0, right = 0;
    int tmp_one = 0, tmp_zero = 0;  // 记录窗口中的 0 1 的数量
    string tmp;

    for (; left < n - 1; right++)
    {
        right %= n;
        tmp += str[right];

        if ('0' == str[right])
            tmp_zero++;
        else if ('1' == str[right])
            tmp_one++;

        while (tmp.size() > n / 2)  // 窗口长度大于字符串长度的一半时退窗口
        {
            if ('0' == tmp[0])
                tmp_zero--;
            else if ('1' == str[left])
                tmp_one--;

            tmp.erase(0, 1);
            left++;
        }

        // 窗口中 0 1 的数量如果是整个串的 0 1 数量的一半时方法数 + 1
        if (tmp_one == one / 2 && tmp_zero == zero / 2)
            ret++;
    }

    cout << ret << endl;

    return 0;
}