#define _CRT_SECURE_NO_WARNINGS

#include <algorithm>

using namespace std;

 struct TreeNode 
 {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
 
    TreeNode(int x) 
        : val(x), left(nullptr), right(nullptr) 
    {}
 };
 
 class Solution
 {
 public:
     int height(TreeNode* root)
     {
         if (nullptr == root)
             return 0;

         return max(height(root->left), height(root->right)) + 1;
     }

     bool IsBalanced_Solution(TreeNode* root)
     {
         if (nullptr == root)
             return true;

         if (abs(height(root->right) - height(root->left)) >= 2)
             return false;

         return IsBalanced_Solution(root->left) && IsBalanced_Solution(root->right);
     }
 };