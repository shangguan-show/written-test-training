#define _CRT_SECURE_NO_WARNINGS

// 1.如何枚举出所有的子矩阵
//	a.如何确定一个矩阵：根据左上角的点 [x1,y1] 和右下角的点 [x2,y2] 即可确定一个矩阵
//	b.使用 4 层 for 循环枚举出 x1 y1 x2 y2
//		for (0 ~ m - 1) -> x1
//			for (0 ~ n - 1) -> y1
//				for (x1 ~ m - 1) -> x2
//					for (y1 ~ n - 1) -> y2
//	b.再求出该矩阵中所有元素的和即可
// 
// 2.如何计算矩阵中所有元素的和
//	a.二维前缀和
//		1.初始化二维 dp 表：dp[i][j] = arr[i][j] + dp[i][j - 1] + dp[i - 1][j] - dp[i - 1][j - 1]
//		2.使用 dp 表：dp[x2][y2] - dp[x1 - 1][y2] - dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1];
// 

#include <vector>
#include <climits>
#include <iostream>
#include <algorithm>

using namespace std;

int n, val;

int main()
{
	cin >> n;
	vector<vector<int>> dp(n + 1, vector<int>(n + 1));
	vector<vector<int>> arr(n + 1, vector<int>(n + 1));

	for (size_t i = 1; i <= n; i++)
	{
		for (size_t j = 1; j <= n; j++)
		{
			cin >> arr[i][j];
			dp[i][j] = arr[i][j] + dp[i][j - 1] + 
						dp[i - 1][j] - dp[i - 1][j - 1];
		}
	}

	// 枚举出所有的子矩阵
	int ret = INT_MIN;

	for (size_t x1 = 1; x1 <= n; x1++)
	{
		for (size_t y1 = 1; y1 <= n; y1++)
		{
			for (size_t x2 = x1; x2 <= n; x2++)
			{
				for (size_t y2 = y1; y2 <= n; y2++)
				{
					ret = max(ret, dp[x2][y2] - dp[x1 - 1][y2] 
						- dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1]);
				}
			}
		}
	}

	cout << ret << endl;

	return 0;
}