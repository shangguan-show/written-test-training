#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <set>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

struct ListNode 
{
	int val;
	struct ListNode *next;

	ListNode(int x) 
		: val(x), next(nullptr) 
	{}
};

class Solution
{
public:
	ListNode* FindFirstCommonNode(ListNode* pHead1, ListNode* pHead2)
	{
		if (nullptr == pHead1 || nullptr == pHead2)
			return nullptr;				// 两个链表必须都不为空

		int lenA = 1, lenB = 1;
		ListNode* curA = pHead1;
		ListNode* curB = pHead2;

		while (curA->next)   			//找链表 A 的尾结点
		{
			lenA++;
			curA = curA->next;
		}

		while (curB->next)   			//找链表 B 的尾结点
		{
			lenB++;
			curB = curB->next;
		}

		if (curA != curB)    			//两条链表不相交
			return nullptr;

		ListNode* longList = pHead1;	//假设长的链表是链表 A
		ListNode* shortList = pHead2;   //假设短的链表是链表 B
		if (lenB > lenA)                //如果链表 B 比链表 A 长
		{
			longList = pHead2;          //长链表成了 B 链表
			shortList = pHead1;         //短链表成了 A 链表
		}

		int n = abs(lenA - lenB);      	//计算两条链表长度的差距

		while (n--)                    	//长的链表先走差距步
			longList = longList->next;

		while (longList != shortList) 	//两条链表从同一起点开始走
		{
			longList = longList->next;
			shortList = shortList->next;
		}

		return longList;				//返回两条链表的交叉结点
	}
};
