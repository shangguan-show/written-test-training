#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <set>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

bool find(const char ch, const string& str2)
{
    for (size_t i = 0; i < str2.size(); i++)
        if (ch == str2[i])
            return true;;
    return false;
}

int main()
{
    string str1;
    string str2;

    getline(cin, str1);
    getline(cin, str2);

    string tmp;

    for (size_t i = 0; i < str1.size(); i++)
    {
        if (!find(str1[i], str2))
            tmp += str1[i];
    }

    cout << tmp << endl;

    return 0;
}