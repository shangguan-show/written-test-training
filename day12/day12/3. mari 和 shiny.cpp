#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <iostream>

using namespace std;

int main()
{
    int n = 0;
    string str;
    cin >> n;

    long long s = 0;    // 统计 s 的个数
    long long sh = 0;   // 统计 sh 的个数
    long long shy = 0;  // 统计 shy 的个数

    for (size_t i = 0; i < n; i++)
    {
        char ch;
        cin >> ch;
        str += ch;
    }

    for (size_t i = 0; i < str.size(); i++)
    {
        if ('s' == str[i])
            s++;
        else if ('h' == str[i])
            sh += s;
        else if ('y' == str[i])
            shy += sh;
    }

    cout << shy << endl;

    return 0;
}