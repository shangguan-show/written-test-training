#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>

using namespace std;

long long n;

int main()
{
    cin >> n;
    vector<long long> arr(n);

    for (size_t i = 0; i < n; i++)
        cin >> arr[i];

    long long count = 0;

    for (size_t i = 1; i < n; i++)
    {
        if (arr[i] < arr[i - 1])        // 进入非递增序列
        {
            count++;

            while (arr[i] <= arr[i - 1] && i < n)
                i++;
        }
        else if (arr[i] > arr[i - 1])   // 进入非递减序列
        {
            count++;

            while (arr[i] >= arr[i - 1] && i < n)
                i++;
        }

        if (i == n - 1)                 // 如果 i 能正好跑到最后一个数，则单独为一个子序列
            count++;
    }

    cout << count << endl;

    return 0;
}