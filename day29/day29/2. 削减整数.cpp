#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <iostream>

using namespace std;

long long t;
long long h;

int main()
{
    cin >> t;

    while (t--)
    {
        cin >> h;

        h -= 1;
        size_t sub = 1;
        size_t count = 1;

        while (h != 0)
        {
            if (0 == h % (2 * sub))
            {
                sub *= 2;
                h -= sub;
            }
            else
            {
                h -= sub;
            }

            count++;
        }

        cout << count << endl;
    }

    return 0;
}