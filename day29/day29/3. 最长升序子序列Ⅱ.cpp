#define _CRT_SECURE_NO_WARNINGS

// 解题思路：贪心 + 二分

#include <vector>
#include <iostream>

using namespace std;

class Solution
{
private:
    int dp[100010] = { 0 }; // dp[i] 表示长度为 i 的最小末尾
    int pos = 0;            // 标记当前填到那个位置
public:
    int LIS(vector<int>& arr)
    {
        
        for (size_t i = 0; i < arr.size(); i++)
        {
            // 查找 arr[i] 应该放在哪个位置
            if (0 == pos || arr[i] > dp[pos])   
            {
                // 刚开始没有元素 或 新来数 > dp 的最后一个数
                dp[++pos] = arr[i];
            }
            else
            {
                // 二分查找插入位置
                int left = 1, right = pos;

                while (left < right)
                {
                    int mid = left + (right - left) / 2;

                    if (dp[mid] >= arr[i])
                        right = mid;
                    else
                        left = mid + 1;
                }

                dp[left] = arr[i];
            }
        }

        return pos;
    }
};