#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

// 解法：排序 + 滑动窗口 + 前缀和 (思想)

// 1.求出两个数的差值在 [0, l - 1] 中的个数 x
// 2.求出两个数的差值在 [0, r - 1] 中的个数 y
// 3.用 y - x 即可得出两个数的差值在 [l, r] 中的个数

int n, l, r;

// 找出差值在 [0, x] 之间一共有多少对
static long long find(vector<int> &arr, int x)
{
	int left = 0, right = 0;
	long long ret = 0;

	for (; right < n; right++)
	{
		while (arr[right] - arr[left] > x)
			left++;
		ret += right - left;
	}

	return ret;
}

int main()
{
	cin >> n >> l >> r;
	vector<int> arr(n);
	for (size_t i = 0; i < n; i++)
		cin >> arr[i];

	sort(arr.begin(), arr.end());

	cout << find(arr, r) - find(arr, l - 1) << endl;

	return 0;
}