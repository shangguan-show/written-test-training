#include <iostream>

using namespace std;

// 解法：数学 - 概率 - 数学期望

// a. 假设抽 3 次，中 2 张 R 的概率，注：每次抽卡抽不中的概率是 0.2，
// 	1.R R X -> 0.8 * 0.8 * 0.2 
//	2.R X R -> 0.8 * 0.2 * 0.8 
//	3.X R R -> 0.2 * 0.8 * 0.8 
// b. 针对上述 3 中情况求出概率，然后累加即可
//	结果为 3 * (0.8)^2 * 0.2
// c.可得出规律为 C * (0.8)^m * (0.2)^(n - m)

int main()
{
	int n = 0, m = 0;
	cin >> n >> m;
	double ret = 1.0;

	for (int i = n; i >= n - m + 1; i--)
		ret *= i;
	for (int i = m; i >= 2; i--)
		ret /= i;
	for (int i = 0; i < m; i++)
		ret *= 0.8;
	for (int i = 0; i < n - m; i++)
		ret *= 0.2;
	
	printf("%.4lf\n", ret);
}