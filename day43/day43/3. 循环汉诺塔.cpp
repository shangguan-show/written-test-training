#include <vector>
#include <iostream>

using namespace std;

// 重点：找出重复子问题

// 1.当 n = 1 时：
//	a.A -> B 需要 1 步
//	b.A -> C 需要 2 步

// 2.当 n = 2 时：
//	a.A -> B：想方设法将最底下的盘子放到 B 上 (前提：将前 n - 1 个盘子先放到 C 上)
//		1.先将 A 柱最顶上的那块盘子 x 经过 2 步转移到 C 柱
//		2.再将最底下的盘子 y 经过 1 步移动到 B 柱，
//		3.最后让 x 盘子走 2 步，从 C 柱移动到 A 柱再移动到 B 柱
//		4.总共移动了 5 步
//	b.A -> C：
//		1.先将 A 柱最顶上的那盘子 x 经过 2 步转移到 C 柱
//		2.再将 A 柱最底下的盘子 y 经过 1 步移动到 B 柱
//		3.将 C 柱上的盘子 x 经过 1 步移动到 A 柱
//		4.将 B 柱上的盘子 y 经过 1 步移动到 C 柱
//		5.将 A 柱上的盘子 x 经过 2 步移动到 C 柱
//		6.总共移动了 7 步

// 3.当 n = 3 时：
//	a.A -> B：前提：将前 n - 1 个盘子先放到 C 上
//		1.该前提的步骤和 n = 2 时的 A -> C 时的步骤一样
//		2.将 A 柱上最底下的盘子移动到 B 柱
//		3.再将 C 柱上的盘子移动到 B 柱和 n = 2 时的 A -> C 时的步骤一样 
//		4.即 n = 3 时，A -> B 的步骤数 = 2 * (n = 2 时 A -> C) 的步骤数 + 1 
//		5.总步数 = 上述 4 步之和
//	b.A -> C：
//		1.将 A 柱上的前 n - 1 个盘子移动到 C 上，步骤数 = n = 2 时的 A -> C 的步骤数 
//		2.将 A 柱上的第 n 个盘子移动到 B 柱上，步骤数 = 前一步的基础上 + 1
//		3.将 C 柱上的 n - 1 个盘子移动到 A 柱上，步骤数 = n = 2 时的 A -> B 的步骤数
//		4.将 B 柱上的第 n 个盘子移动到 C 柱上，步骤数 = 前一步的基础上 + 1
//		5.将 A 柱上的 n - 1 个盘子移动到 C 柱上，步骤数 = n = 2 时的 A -> C 的步骤数 
//		6.总步数 = 上述 5 步之和

int n;
const int mod = 1e9 + 7;

int main()
{
	cin >> n;
	int x = 1;	// 初始只有 1 个盘子时 A -> B 时的步数，记录 A -> B 的步数
	int y = 2;	// 初始只有 1 个盘子时 A -> C 时的步数，记录 A -> C 的步数

	for (int i = 2; i <= n; i++)
	{
		int xx = x, yy = y;
		x = (2 * yy + 1) % mod;
		y = ((2 * yy) % mod + 2 + xx) % mod;
	}

	cout << x << " " << y << endl;

	return 0;
}