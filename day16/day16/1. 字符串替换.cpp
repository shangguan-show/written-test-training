#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <cctype>
#include <vector>

using namespace std;

class Solution
{
public:
    string formatString(string str, vector<char>& arg)
    {
        size_t j = 0;
        string ret;

        // 将 str 和 arg 中的字母尾插进 ret 串
        for (size_t i = 0; i < str.size();)
        {
            // 是字母就尾插到 ret 串中
            if (isalpha(str[i]))
            {
                ret += str[i];
                i++;
            }
            // 不是字母就表示碰到了 %，跳过 %s 这两个字符，然后用 arg 中的字母替换
            else
            {
                i += 2;
                ret += arg[j++];
            }
        }

        // arg 中的字符还未全部添加到 ret 串时，全部尾插进去
        while (j < arg.size())
            ret += arg[j++];

        return ret;
    }
};