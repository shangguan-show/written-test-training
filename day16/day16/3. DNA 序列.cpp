#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <string>
#include <iostream>

using namespace std;

string fun(string& str, double len)
{
    int index = 0;
    int max_GCRatio = 0;

    for (int left = 0; left < str.size() - len; left++)
    {
        int countGC = 0;    // 统计 G C 的个数

        for (int right = left; right < left + len; right++)
        {
            if ('G' == str[right])
                countGC++;
            else if ('C' == str[right])
                countGC++;
        }

        if (max_GCRatio < countGC)
        {
            max_GCRatio = countGC;
            index = left;
        }
    }

    return str.substr(index, len);
}

int main()
{
    string str;
    double len = 0;

    cin >> str;
    cin >> len;

    cout << fun(str, len) << endl;

    return 0;
}