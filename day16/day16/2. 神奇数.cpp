#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

// 判断是否是质数
bool prime_number(long long val)
{
    for (size_t i = 2; i <= sqrt(val); i++)
        if (0 == val % i)
            return false;

    return true;
}

// 判断是否是神奇数
bool magic_number(long long val)
{
    // 存储 val 中的每一位数字
    vector<long long> tmp;

    while (val)
    {
        tmp.push_back(val % 10);
        val /= 10;
    }

    // 判断该数组中的任意不相等组合是否为质数
    for (size_t i = 0; i < tmp.size(); i++)
        for (size_t j = 0; j < tmp.size(); j++)
            if (i != j && tmp[i])
                if (prime_number(tmp[i] * 10 + tmp[j]))
                    return true;

    return false;
}

int main()
{
    long long a = 0, b = 0;
    cin >> a >> b;

    long long count = 0;

    for (; a <= b; a++)
        if (magic_number(a))
            count++;

    cout << count << endl;

    return 0;
}