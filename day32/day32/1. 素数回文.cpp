#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

static bool is_prime(long long n)
{
    if (0 == n || 1 == n)
        return false;

    for (size_t i = 2; i <= sqrt(n); i++)
        if (0 == n % i)
            return false;

    return true;
}

static bool is_back_prime(long long n)
{
    long long back1 = n, back2 = n;

    // 例：将 123 弄成 123321
    for (long long tmp = n; tmp; tmp /= 10)
        back1 = back1 * 10 + tmp % 10;

    // 例： 将 123 弄成 12321
    for (long long tmp = n / 10; tmp; tmp /= 10)
        back2 = back2 * 10 + tmp % 10;

    // 判断这两类情况有没有至少一个是回文数
    if (is_prime(back1) || is_prime(back2))
        return true;

    return false;
}

int main()
{
    long long t = 0;
    cin >> t;

    if (is_back_prime(t))
        cout << "prime" << endl;
    else
        cout << "noprime" << endl;

    return 0;
}