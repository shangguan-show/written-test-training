#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

struct cmp
{
    bool operator()(vector<long long> a, vector<long long> b)
    {
        return a[1] <= b[1];
    }
};

int main()
{
    int n = 0;
    cin >> n;

    vector<vector<long long>> arr(n, vector<long long>(2));

    for (size_t i = 0; i < n; i++)
        cin >> arr[i][0] >> arr[i][1];

    long long count = 1;
    sort(arr.begin(), arr.end(), cmp());

    for (size_t i = 1; i < n; i++)
        if (arr[i][0] >= arr[i - 1][1])
            count++;

    cout << count << endl;

    return 0;
}