#define _CRT_SECURE_NO_WARNINGS

/* 解题思路：动态规划 - 线性 dp */

// 1.状态表示
//	a.dp[i][j] 表示从 [1, i] 中挑选，挑 j 个人，最后一个人 arr[i] 必选，此时的最大乘积是多少
//	b.如果 arr[i] 是负数，那么得知道 [1, i -1] 中得最小乘积和 arr[i] 相乘才能最大
// 本题得状态表示如下所示：
//	c.f[i][j] 表示从 [1, i] 中挑 j 个人，最后一个人必选，此时得最大乘积
//	d.g[i][j] 表示从 [1, i] 中挑 j 个人，最后一个人必选，此时得最小乘积

// 2.状态转移方程
//	a.第 i 个人与前一个人得下标之差不能超过 d，i - prev <= d -> prev >= i - d
//	b.从 [1, prev] 中挑选 j - 1 个人得最大乘积与 i 处得相乘
//	c.prev 得取值范围：max(i - d, j - 1) <= prev <= i - 1
//	d.f[i][j] = 下面两种情况得最大值
//		1.arr[i] > 0 时得最大值：f[prev][j - 1] * arr[i];
//		2.arr[i] < 0 时得最大值：g[prev][j - 1] * arr[i];
//	e.g[i][j] = 下面两种情况得最小值
//		1.arr[i] > 0 时得最大值：f[prev][j - 1] * arr[i];
//		2.arr[i] < 0 时得最大值：g[prev][j - 1] * arr[i];

// 3.初始化
//	a.因为需要从前 i 个人中挑 j 个人，所以只用在 i >= j 时挑人即可
//	b.第一列可能越界，直接将 f 和 g 表的第一列初始化成 arr 第一列对应得值 
//	c.将 f 表中 i >= j 且非第一列的格子初始化成 -∞
//	d.将 g 表中 i >= j 且非第一列的格子初始化成 +∞

// 4.填表顺序
//	a.从上往下，从左往右

// 5.返回值
//	a.return max(f[n][k] ~ f[k][k]);

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int n, k, d;
const long long INF = 0x3f3f3f3f3f3f3f3f;		// 定义无穷大

int main()
{
	// 录入数据
	cin >> n;

	vector<long long> arr(n + 1);
	for (size_t i = 1; i <= n; i++)
		cin >> arr[i];

	cin >> k >> d;

	// 1.创建 dp 表
	vector<vector<long long>> f(55, vector<long long>(15));
	vector<vector<long long>> g(55, vector<long long>(15));

	// 2.在填表的过程中初始化
	for (int i = 1; i <= n; i++)
	{
		f[i][1] = g[i][1] = arr[i];				// 初始化两个表的第一列

		for (int j = 2; j <= min(i, k); j++)	// 挑选 j 个人
		{
			f[i][j] = -INF;
			g[i][j] = +INF;

			for (int prev = max(i - d, j - 1); prev <= i - 1; prev++)
			{
				f[i][j] = max(f[i][j], max(f[prev][j - 1] * arr[i], g[prev][j - 1] * arr[i]));
				g[i][j] = min(g[i][j], min(f[prev][j - 1] * arr[i], g[prev][j - 1] * arr[i]));
			}
		}
	}

	// 3.确定返回值
	long long ret = -INF;

	for (size_t i = k; i <= n; i++)
		ret = max(ret, f[i][k]);

	cout << ret << endl;

	return 0;
}